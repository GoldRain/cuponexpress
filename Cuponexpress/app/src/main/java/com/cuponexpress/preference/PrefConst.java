package com.cuponexpress.preference;

/**
 * Created by ToSuccess on 11/1/2016.
 */

public class PrefConst {

    public static final String PREFKEY_TOKEN = "tokenid";
    public static final String PREFKEY_USEREMAIL = "email";
    public static final String PREFKEY_XMPPID = "XMPPID";
    public static final String PREFKEY_USERNAME = "username";
    public static final String PREFKEY_USERPWD = "password";
    public static final String PREFKEY_PHOTURL= "photourl";
    public static final String PREFKEY_PHOTURL_L = "photourl";
    public static final String PREFKEY_ID= "id";

    public static final String PREFKEY_RECORDEDATE1_TITLE1 = "title";
    public static final String PREFKEY_RECORDEDATE1_TIME1 = "time";
    public static final String PREFKEY_RECORDEDATE1_YEAR1 = "year";

    public static final String PRFKEY_LASTLOGINID = "lastlogin_id";
}
