package com.cuponexpress.custom_dialog;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.EditText;

import com.cuponexpress.application.ValleyApplication;

/**
 * Created by ToSuccess on 10/29/2016.
 */

public class dialog_EditText extends EditText {
    public dialog_EditText(Context context , AttributeSet attrs, int defStyle) {

        super(context, attrs, defStyle);
        init();
    }

    public dialog_EditText (Context context, AttributeSet attrs){

        super(context, attrs);
        init();
    }

    public dialog_EditText (Context context) {

        super(context);
        init();
    }

    public void init() { setTypeface(ValleyApplication.DroidFace, 1);}
}
