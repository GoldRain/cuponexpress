package com.cuponexpress.custom_dialog;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.TextView;

import com.cuponexpress.application.ValleyApplication;

/**
 * Created by ToSuccess on 10/29/2016.
 */

public class dialog_TextView extends TextView  {


    public dialog_TextView(Context context) {

        super(context);

        init();
    }

    public dialog_TextView(Context context, AttributeSet attrs) {

        super(context, attrs);

        init();
    }

    public dialog_TextView(Context context, AttributeSet attrs, int defStyleAttr) {

        super(context, attrs, defStyleAttr);

        init();
    }


    public void init() {

        setTypeface(ValleyApplication.DroidFace, 1);
    }
}
