package com.cuponexpress.custom_dialog;

import android.content.Context;
import android.os.Handler;
import android.util.AttributeSet;
import android.widget.Button;

import com.cuponexpress.application.ValleyApplication;
import com.cuponexpress.main.MainActivity;

import static com.cuponexpress.R.id.time;

/**
 * Created by ToSuccess on 10/29/2016.
 */

public class dialog_Button extends Button {

    public dialog_Button(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public dialog_Button(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public dialog_Button(Context context) {
        super(context);
        init();
    }

    public void init() {
        setTypeface(ValleyApplication.ButtonFace, 1);
    }
}
