package com.cuponexpress.custom_dialog;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.NumberPicker;
import android.widget.TextView;

import com.cuponexpress.application.ValleyApplication;

/**
 * Created by ToSuccess on 10/29/2016.
 */

public class dialog_NumberPicker extends NumberPicker {

    public dialog_NumberPicker(Context context , AttributeSet attts) {

        super(context);
        setDescendantFocusability(NumberPicker.FOCUS_AFTER_DESCENDANTS);
    }

    @Override
    public void addView(View child) {
        super.addView(child);
        updateView(child);
    }

    @Override
    public void addView(View child, int index, ViewGroup.LayoutParams params) {

        super.addView(child, index, params);
        updateView(child);
    }

    @Override
    public void addView(View child, ViewGroup.LayoutParams params) {

        super.addView(child, params);
        updateView(child);
    }

    public void updateView(View view) {

        if (view instanceof EditText) {

            // ((EditText) view).setTextSize(25);
            ((EditText) view).setTypeface(Typeface.createFromAsset(getContext()
                    .getAssets(), ValleyApplication.Droid_LOCATION_PATH_en));

        } else if (view instanceof TextView) {

            ((TextView) view).setTypeface(ValleyApplication.DroidFace);
        }
    }
}
