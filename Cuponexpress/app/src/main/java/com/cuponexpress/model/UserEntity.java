package com.cuponexpress.model;

import java.io.Serializable;

/**
 * Created by ToSuccess on 11/3/2016.
 */

public class UserEntity implements Serializable {

    int _idx = 0 ;
    String _username = "";
    String _email = "";
    String _password = "";
    String _confirmPassword = "";
    String _photoUrl = "";


    public int get_idx() {return _idx;}

    public void set_idx(int _idx) {this._idx = _idx;}

    public String get_username() {return _username;}

    public void set_username(String _username) {this._username = _username;}

    public String get_email() {return _email;}

    public void set_email(String _email) {this._email = _email;}

    public String get_password() {return _password;}

    public void set_password(String _password) {this._password = _password;}

    public String get_confirmPassword() {return _confirmPassword;}

    public void set_confirmPassword(String _confirmPassword) {this._confirmPassword = _confirmPassword;}

    public String get_photoUrl() {return _photoUrl;}

    public void set_photoUrl(String _photoUrl) {this._photoUrl = _photoUrl;}



}
