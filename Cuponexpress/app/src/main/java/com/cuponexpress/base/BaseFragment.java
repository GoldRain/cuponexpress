package com.cuponexpress.base;

import android.support.v4.app.Fragment;
import android.widget.Toast;

/**
 * Created by ToSuccess on 10/23/2016.
 */

public abstract class BaseFragment  extends Fragment{



    public BaseActivity _context;

    public void showProgress(){

        _context.showProgress();
    }

    public void CloseProgress(){

        _context.closeProgress();
    }

    public void showToast(String strMsg){

        Toast.makeText(_context , strMsg, Toast.LENGTH_SHORT).show();

        //_context.showToast(strMsg);
    }

    public void showAlert(String strMsg){

        _context.showAlertDialog(strMsg);
    }
}