package com.cuponexpress.base;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.KeyEvent;
import android.widget.TextView;

import com.cuponexpress.commons.Commons;

/**
 * Created by ToSuccess on 10/23/2016.
 */

public class CommonTabActivity extends CommonActivity {

    protected TextView ui_txvUnread ;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Commons.g_currentActivity = this ;

    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {

        if (keyCode == KeyEvent.KEYCODE_BACK){

            Commons.g_isAppRunning = false ;

        }
        return super.onKeyDown(keyCode, event);
    }
}
