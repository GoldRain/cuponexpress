package com.cuponexpress.base;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Vibrator;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.ActionMode;
import android.widget.Button;
import android.widget.Toast;

import com.cuponexpress.R;

/**
 * Created by ToSuccess on 10/23/2016.
 */

public class BaseActivity extends AppCompatActivity implements Handler.Callback {

    public Context _context = null;
    public Handler _handler = null;
    public ProgressDialog _progressDlg;
    public Vibrator _vibrator;
    public boolean _isEndFlag ;
    public static final int BACK_TWO_CLICK_DELAY_TIME = 3000; //ms


    public Runnable _exitRunner = new Runnable() {
        @Override
        public void run() {
            _isEndFlag = false ;
        }
    };

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        _context = this;
        _vibrator = (Vibrator)getSystemService(VIBRATOR_SERVICE);
        _handler = new Handler(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onDestroy() {

        closeProgress();

        try {
            if (_vibrator != null)

                _vibrator.cancel();
            } catch (Exception e){

        }
        _vibrator = null;
        super.onDestroy();
    }

    public void showProgress(boolean cancelable){

        closeProgress();

        _progressDlg = new ProgressDialog(_context, R.style.MyTheme);
        _progressDlg.setProgressStyle(android.R.style.Widget_ProgressBar_Large);
        _progressDlg.setCancelable(cancelable);
        _progressDlg.show();
    }

    public void showProgress() {showProgress(false);}

    public void closeProgress() {

        if (_progressDlg == null){

            return;
        }

        _progressDlg.dismiss();
        _progressDlg = null;
    }

    public void showAlertDialog(String msg) {

        final AlertDialog alertDialog = new AlertDialog.Builder(_context).create();

        alertDialog.setTitle(getString(R.string.app_name));
        alertDialog.setMessage(msg);

        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE,_context.getString(R.string.ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        });

        alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialogInterface) {
                Button b = alertDialog.getButton(DialogInterface.BUTTON_POSITIVE);
                b.setTextColor(getResources().getColor(R.color.text_color));
            }
        });

        alertDialog.show();
    }

    public void showToast(String toast_string) {

        Toast.makeText(_context , toast_string, Toast.LENGTH_SHORT).show();
    }

    public void vibrate(){
        if (_vibrator != null){

            _vibrator.vibrate(500);
        }
    }

    @Override
    public boolean handleMessage(Message message) {

        switch (message.what){
            default:
                break;
        }
        return false;
    }

    public void onExit(){

        if (_isEndFlag == false){

            Toast.makeText(this , getString(R.string.str_back_one_more_end),Toast.LENGTH_SHORT).show();
            _isEndFlag = true;
            _handler.postDelayed(_exitRunner , BACK_TWO_CLICK_DELAY_TIME);

        } else if (_isEndFlag == true){

            finish();
        }
    }
}
