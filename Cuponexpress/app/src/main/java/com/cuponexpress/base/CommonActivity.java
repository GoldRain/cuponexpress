package com.cuponexpress.base;

import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.os.Bundle;
import android.support.annotation.Nullable;

import java.util.List;
import java.util.Locale;

/**
 * Created by ToSuccess on 10/23/2016.
 */

public class CommonActivity extends BaseActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    private String getLauncherClassName(){

        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_LAUNCHER);
        intent.setPackage(getPackageName());

        List<ResolveInfo> resolveInfoList = getPackageManager().queryIntentActivities(intent, 0);
        if(resolveInfoList != null && resolveInfoList.size() > 0) {
            return resolveInfoList.get(0).activityInfo.name;
        }

        return null;
    }


    public String getLanguage(){

        Locale systemLocale = getResources().getConfiguration().locale;
        String strLanguage = systemLocale.getLanguage();

        return strLanguage;
    }

    @Override
    protected void onUserLeaveHint() {
        super.onUserLeaveHint();
//        Commons.g_isAppPaused = true;
    }

    @Override
    protected void onResume() {
        super.onResume();
//        Commons.g_isAppPaused = false;
//        Commons.g_isAppRunning = true;
    }

}
