package com.cuponexpress.commons;

import java.net.PortUnreachableException;

/**
 * Created by ToSuccess on 10/23/2016.
 */

public class Constants {

    public static final int VOLLEY_TIME_OUT = 6000 ;
    public static final int SPLASH_TIME = 4600 ;
    /*public static final int SPLASH_TIME = 2000 ;*/
    public static final int DRAWER_CLOSE_DELAY_MS = 350 ;
    public static final int PROFILE_IMAGE_SIZE = 256 ;

    public static String token=null;
    public static String receivemessage="";
    public static int messagetype=0;  //0: word, 1: image  3: product messsage
    public static String recieveimageurl="";


    public static boolean messagestatuse;

    public static final String USER = "user";
    public static final String PASSWORD = "password";
    public static final String EMAIL = "email";
    public static final String KEY_LOGOUT = "logout";

    public static final int CUPONES = 1;
    public static final int FAVPROTS = 2;
    public static final int SHOPPING = 3;
    public static final int FAVORITE = 4;

    public static final int PICK_FROM_CAMERA = 100;
    public static final int PICK_FROM_ALBUM = 101;
    public static final int CROP_FROM_CAMERA = 102;

}
