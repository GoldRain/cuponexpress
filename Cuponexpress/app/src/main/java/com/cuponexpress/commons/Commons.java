package com.cuponexpress.commons;

import android.os.Handler;

import com.cuponexpress.base.CommonActivity;
import com.cuponexpress.model.UserEntity;

/**
 * Created by ToSuccess on 10/23/2016.
 */

public class Commons {

    public static boolean g_isAppRunning = false ;
    public static boolean g_isAppPaused = false ;

    public static Handler g_handler = null ;
    public static String g_appVersion = "1.0";

    public static UserEntity g_newUser = null ;
    public static UserEntity g_user = new UserEntity() ;

    public static int status = 0 ;
    public static String _title = "";


    public static CommonActivity g_currentActivity = null;
}
