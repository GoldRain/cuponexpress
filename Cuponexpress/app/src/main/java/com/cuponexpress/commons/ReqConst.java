package com.cuponexpress.commons;

import com.cuponexpress.base.CommonActivity;
import com.cuponexpress.model.UserEntity;

/**
 * Created by ToSuccess on 10/23/2016.
 */

public class ReqConst {

    //public static final String SERVER_ADDR = "http://35.162.10.72/Coupon";
    public static final String SERVER_ADDR = "http://35.167.195.131";

    public static boolean g_isAppRunning = false ;
    public static boolean g_isAppPaused = false ;

    public static UserEntity g_newUser = null;
    public static UserEntity g_user = null;

    public static CommonActivity g_currentActivity = null;



    public static final String SERVER_URL = SERVER_ADDR + "/index.php/Api/" ;

    //Request value
    public static final String REQ_LOGIN = "login";
    public static final String REQ_UPLOADIMAGE = "uploadimage";
    public static final String REQ_UPDATEPROFILE = "updateProfile";
    public static final String  REQ_SIGNUP = "signup";
    public static final String REQ_WITHFACEBOOK = "loginwithfacebook";
    public static final String REQ_RECOVERYPWD ="recoverpassword";
    public static final String REQ_CHANGEPWD ="changepassword";
    public static final String REQ_RECORDDATE ="recorddate";
    public static final String REQ_GETUSERRECODE = "getUserRecordDate";
    public static final String REQ_REPORT ="report";



    public static final String  REQ_FACEBOOK ="loginwithfacebook";

    //response value

    public static final String RES_CODE = "result_code";
    public static final String RES_USERINFO = "user_info";
    public static final String RES_ID = "id";
    public static final String RES_IDX = "idx";
    public static final String RES_USERNAME = "username";
    public static final String RES_EMAIL = "email";
    public static final String RES_PWD = "password";
    public static final String RES_PHOTO_URL = "photo_url";
    public static final String RES_RECORDDATE = "record_dates";
    public static final String RES_SEQUENCE = "sequencce";
    public static final String RES_TITLE = "title";
    public static final String RES_DATATIME = "dateTime";




    //request params
    public static final String PARAM_EMAIL = "email";
    public static final String PARAM_PASSWORD = "password";
    public static final String PARAM_TYPE = "type";
    public static final String PARAM_FILENAME = "filename";
    public static final String PARAM_NAME = "name";
    public static final String PARAM_PHONE_NUMBER = "phone_number";
    public static final String PARAM_PHOTO = "photo";
    public static final String PARAM_NEW_PASSWORD = "new_password";

    public static final String PARAM_ID = "id";
    public static final String PARAM_FILE = "file";


    public static final int CODE_SUCCESS = 200;
    public static final int CODE_EXITNAME = 206;
    public static final int CODE_EXISTEMAIL = 207;
    public static final int CODE_UNREGUSER = 209;
    public static final int CODE_WRONGPWD = 210;
    public static final int CODE_UNKNOWNERRO = 208 ;
    public static final int CODE_UPLOADFAIL = 211;
    public static final int CODE_NOTEMAIL = 107;
}
