package com.cuponexpress.application;

import android.app.Application;
import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Typeface;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.widget.TextView;

/**
 * Created by ToSuccess on 10/29/2016.
 */

public class ValleyApplication extends Application {


    public static byte[] vid;

    public static final String TAG = ValleyApplication.class.getSimpleName();

    private static ValleyApplication mInstance;
    public static Typeface DroidFace;
    public static Typeface ButtonFace;

    public static String Droid_LOCATION_PATH_ar = "fonts/eurosti.TTF";
    public static String Droid_LOCATION_PATH_en = "fonts/eurostib.TTF";

    public static Context context;
    public static int move = 0;
    public static int mainGallaryPosition = 0;
    public static int commentNo = 0;
    public static String phoneno = "";
    public static boolean lang_changed = false;
    public static String mCurrentPhotoPath = null;
    public static int tabsize = 15;
    public static int tabsize_detail = 15;
    public static int Symobl_type = 2;
    public static int price_type = 1;
    public static int percentage_value = 1;
    public static int fragment_number = 1;
//    public static Stock WidgetDetailID = new Stock();
//    // public static String sks = "2014@PuorGehT";
//    public static Customerdata cusdata = new Customerdata();
//    public static Sahl_Login sahlLogin = new Sahl_Login();
//    public static ConfigData config = new ConfigData();
    public static boolean isLoged;
    public static boolean isLoged_sahl;
    public static boolean refresh1 = true;
    public static String OrderType;
    public static String Market_Close = "1";
    public static String Market_Open = "2";
    public static String Market_Pre_Close = "3";
    public static String Market_Pre_Open = "4";
    public static String Market_Trading_At_Last = "5";
    public static String apikey = "12x3Fg893";
    public static String SessionID = "";

//    4tC8Au5sZAZX097Xdj2XvAh1yGb2DX2Mq33QgQLz
//    qmbxvBoragTafBAyrxXRqJ3vyin8kOpzZBIqWCfu
//    public static final String appID = "0dsG8sreUj6mLEdbKg1vhyrjGbsvgkSlZW5thPuq";
//    public static final String clientKey = "8U0djKNYdhprJzjjxap6XAfjwkvVPodGSbhTPi8h";

    // check to enable touch id setting on the current device
    public static boolean isEnableTouchID = false;
    // if touch id is set on the current device, or not
    public static boolean isSetTouchID = false;

    public static synchronized ValleyApplication getInstance() {
        return mInstance;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        mInstance = this;
        context = getApplicationContext();

        // initialize parse
//        Parse.initialize(context, appID, clientKey);
//        PushService.subscribe(context, "global", NotificationActivity.class);
//        PushService.setDefaultPushCallback(context, NotificationActivity.class);

        initFonts(getAssets());

        // initialize reprint for TouchID

//        Reprint.initialize(this);

//        isEnableTouchID = (Reprint.isHardwarePresent() && Reprint.hasFingerprintRegistered());
    }

    /**
     *  load fonts
     * @param assetManager
     */
    public static void initFonts(AssetManager assetManager) {
//        if(Language.getLanguage().equals(Language.lang_ar)) {
//            DroidFace = Typeface.createFromAsset(assetManager, Droid_LOCATION_PATH_ar);
//            ButtonFace = Typeface.createFromAsset(assetManager, Droid_LOCATION_PATH_ar);
//        } else {

            DroidFace = Typeface.createFromAsset(assetManager, Droid_LOCATION_PATH_en);
            ButtonFace = Typeface.createFromAsset(assetManager, Droid_LOCATION_PATH_en);
//        }
    }

    /**
     *  apply custom font all child view in the view group
     * @param list
     * @param customTypeface
     */
    public static void applyCustomFont(ViewGroup list, Typeface customTypeface) {
        for (int i = 0; i < list.getChildCount(); i++) {
            View view = list.getChildAt(i);
            if (view instanceof ViewGroup) {
                applyCustomFont((ViewGroup) view, customTypeface);
            } else if (view instanceof TextView) {
                ((TextView) view).setTypeface(customTypeface);
            }
        }
    }

    public static void expand(final View v) {
        v.measure(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        final int targetHeight = v.getMeasuredHeight();

        v.getLayoutParams().height = 0;
        v.setVisibility(View.VISIBLE);
        Animation anim = new Animation() {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                v.getLayoutParams().height = interpolatedTime == 1 ? ViewGroup.LayoutParams.WRAP_CONTENT
                        : (int) (targetHeight * interpolatedTime);
                v.requestLayout();
            }

            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };

        // 1dp per 1ms
        anim.setDuration((int)(targetHeight/v.getContext().getResources().getDisplayMetrics().density));
        v.startAnimation(anim);
    }

    public static void collapse(final View v) {
        final int initialHeight = v.getMeasuredHeight();

        Animation anim = new Animation() {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                if (interpolatedTime == 1) {
                    v.setVisibility(View.GONE);
                } else {
                    v.getLayoutParams().height = initialHeight
                            - (int) (initialHeight * interpolatedTime);
                    v.requestLayout();
                }
            }

            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };

        // 1dp/ms
        anim.setDuration((int) (initialHeight / v.getContext().getResources()
                .getDisplayMetrics().density));
        v.startAnimation(anim);
    }
}
