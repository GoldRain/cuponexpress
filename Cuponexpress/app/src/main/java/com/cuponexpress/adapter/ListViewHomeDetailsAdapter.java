package com.cuponexpress.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.cuponexpress.R;
import com.cuponexpress.main.MainActivity;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by ToSuccess on 12/8/2016.
 */

public class ListViewHomeDetailsAdapter extends BaseAdapter{

    MainActivity _activity ;
    ViewPagerHome_DetailsAdapter adapter;

    public ListViewHomeDetailsAdapter(MainActivity activity){
        this._activity = activity;
    }

    @Override
    public int getCount() {
        return 10;
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        HomedetailsHolder homedetailsHolder ;

        if (convertView == null){

            homedetailsHolder = new HomedetailsHolder();

            LayoutInflater inflater = (LayoutInflater) _activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.item_list_review, parent, false);

            homedetailsHolder.ui_imvPhoto = (CircleImageView)convertView.findViewById(R.id.circleImageView);
            homedetailsHolder.ui_txvName = (TextView)convertView.findViewById(R.id.txv_name_det);
            homedetailsHolder.ui_txvContent_a = (TextView)convertView.findViewById(R.id.txv_content_abov);
            homedetailsHolder.ui_txvContent_b = (TextView)convertView.findViewById(R.id.txv_content_down);

            convertView.setTag(homedetailsHolder);

        } else {

            homedetailsHolder = (HomedetailsHolder)convertView.getTag();
        }

        return convertView;
    }

    public class HomedetailsHolder {

        CircleImageView ui_imvPhoto;
        TextView ui_txvName;
        TextView ui_txvContent_a ;
        TextView ui_txvContent_b ;
    }
}
