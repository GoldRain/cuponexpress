package com.cuponexpress.adapter;

import android.content.Context;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.cuponexpress.R;
import com.cuponexpress.main.MainActivity;

import java.util.ArrayList;

/**
 * Created by ToSuccess on 11/18/2016.
 */

public class GridView_CategoriesAdapter extends BaseAdapter {

    MainActivity _activity;
    View view ;
    ArrayList<Integer> _cells = new ArrayList<>();
    ArrayList<String> _name = new ArrayList<>();

    public GridView_CategoriesAdapter(MainActivity activity,ArrayList<Integer> cells, ArrayList<String> name){

        this._activity = activity ;
        this._cells = cells ;
        this._name = name ;
    }

    @Override
    public int getCount() {
        return 10;
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        Holder holder ;

        if (convertView == null){

            holder = new Holder();

            LayoutInflater inflater = (LayoutInflater)_activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            convertView = inflater.inflate(R.layout.item_gdv_categorias, parent, false);

            holder.ui_imvCell = (ImageView)convertView.findViewById(R.id.imv_cell_cat);
            holder.ui_imvCell.setImageResource(_cells.get(position));

            holder.ui_name_cat = (TextView)convertView.findViewById(R.id.txv_name_cat);
            holder.ui_name_cat.setText(_name.get(position));

            convertView.setTag(holder);

        } else {

            holder = (Holder)convertView.getTag();
        }

     /*   convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {



            }
        });*/

        return convertView;
    }

    public class Holder {
        public ImageView ui_imvCell ;
        public TextView ui_name_cat ;
    }
}
