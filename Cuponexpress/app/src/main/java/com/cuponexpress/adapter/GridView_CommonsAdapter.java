package com.cuponexpress.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.cuponexpress.R;
import com.cuponexpress.commons.Commons;
import com.cuponexpress.commons.Constants;
import com.cuponexpress.main.MainActivity;

import java.util.ArrayList;

/**
 * Created by ToSuccess on 10/28/2016.
 */

public class GridView_CommonsAdapter extends BaseAdapter{

    MainActivity _activity ;

    ArrayList<Integer> _img = new ArrayList<>();

    public GridView_CommonsAdapter(MainActivity activity, ArrayList<Integer> img)
    {
        this._activity = activity ;
        this._img = img ;
    }

    @Override
    public int getCount() {
        return _img.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        Holder holder;

        if (convertView == null){

            holder = new Holder();

            LayoutInflater inflater = (LayoutInflater)_activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            convertView = inflater.inflate(R.layout.item_common_gridview, parent, false );

            holder.imvProduct = (ImageView)convertView.findViewById(R.id.imv_products);
            holder.imvProduct.setImageResource(_img.get(position));

            holder.txvTitle = (TextView)convertView.findViewById(R.id.txv_title);
            holder.txvTitle.setText("COCOICON");

            convertView.setTag(holder);

        } else {

            holder = (Holder)convertView.getTag();
        }

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

//                if (Commons.status == Constants.CUPONES){
//
//                    _activity.showFragment();
//                    Commons.status  = 1;
//
//                } else if (Commons.status == Constants.SHOPPING){
//
//                    _activity.showFragment();
//                    Commons.status = 3;
//
//                } else if (Commons.status == Constants.FAVORITE){
//
//                    _activity.showFragment();
//                    Commons.status = 4;
//
//                } else {}

              /*  _activity.showFragment();*/
            }
        });

        return convertView;
    }

    public class Holder {

        public ImageView imvProduct;
        public TextView txvTitle;
    }
}
