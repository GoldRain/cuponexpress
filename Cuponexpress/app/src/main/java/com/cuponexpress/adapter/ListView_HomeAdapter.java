package com.cuponexpress.adapter;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.cuponexpress.R;
import com.cuponexpress.main.MainActivity;

import java.util.ArrayList;

/**
 * Created by ToSuccess on 10/25/2016.
 */

public class ListView_HomeAdapter extends BaseAdapter {

    MainActivity activity;

    ViewPagerAdapterHome adapterHome;

    RecyclerView ui_recyclerImage ;
    HomeRecyclerViewAdapter _recyclerViewAdapter ;

    ArrayList<Integer> _imgs = new ArrayList<>();
    ArrayList<Integer> _imgs2 = new ArrayList<>();
    ArrayList<String> _title = new ArrayList<>();

    ViewPager ui_vip;

    TextView ui_txvTitle;

    public ListView_HomeAdapter (MainActivity activity, ArrayList<Integer> imgs, ArrayList<String> title)

    {
        this.activity = activity ;
        this._imgs = imgs;
        /*this._imgs2 = imgs2 ;*/
        this._title = title;

        loadData();
    }

    private void loadData() {

        _imgs.add(R.drawable.img_c1); _imgs.add(R.drawable.img_c2); _imgs.add(R.drawable.img_c3); _imgs.add(R.drawable.img_c4);
        _imgs.add(R.drawable.img_c5); _imgs.add(R.drawable.img_c6); _imgs.add(R.drawable.img_c7); _imgs.add(R.drawable.img_c8);
        _imgs.add(R.drawable.img_c9); _imgs.add(R.drawable.img_c2); _imgs.add(R.drawable.img_c3); _imgs.add(R.drawable.img_c4);

    }


    @Override
    public int getCount() {
       return 3 ;}

    @Override
    public Object getItem(int postion) {
        return postion;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder viewHolder ;

        if (convertView == null){

            viewHolder = new ViewHolder();

            LayoutInflater inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.item_home_listview, parent, false);

           /* ui_vip = (ViewPager)convertView.findViewById(R.id.vpglst_home);
            adapterHome = new ViewPagerAdapterHome(activity, imgs, imgs2);
            ui_vip.setAdapter(adapterHome);*/

         /*   ui_rev = (RecyclerView)convertView.findViewById(R.id.rev_image);
            _homeRecyclerViewAdapter = new HomeRecyclerViewAdapter(activity, imgs);
            ui_rev.setAdapter(_homeRecyclerViewAdapter);*/

            ui_recyclerImage = (RecyclerView)convertView.findViewById(R.id.rev_image);
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false);
            ui_recyclerImage.setLayoutManager(linearLayoutManager);

            _recyclerViewAdapter = new HomeRecyclerViewAdapter(activity, _imgs);
            ui_recyclerImage.setAdapter(_recyclerViewAdapter);

            ui_txvTitle = (TextView)convertView.findViewById(R.id.txv_title_home);
            ui_txvTitle.setText(_title.get(position));

            convertView.setTag(viewHolder);

        } else {
            viewHolder = (ViewHolder)convertView.getTag();
        }

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        return convertView;
    }

    public class ViewHolder {

        TextView txvTitle ;
    }
}
