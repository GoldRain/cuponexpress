package com.cuponexpress.adapter;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.cuponexpress.R;
import com.cuponexpress.main.MainActivity;

import java.util.ArrayList;

/**
 * Created by ToSuccess on 10/28/2016.
 */

public class ViewPagerCategAdapter  extends PagerAdapter{

    MainActivity _activity ;
    LayoutInflater mLayoutInflater;
    ArrayList<Integer> tiltls ;

    public ViewPagerCategAdapter (MainActivity activity, ArrayList<Integer> tiltls){

        this._activity = activity ;
        this.tiltls = tiltls ;

        mLayoutInflater = (LayoutInflater)_activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    @Override
    public int getCount() {
        return 5;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {


        return view == ((LinearLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {

        View itemView = mLayoutInflater.inflate(R.layout.item_viewpager_categ ,container, false);

        container.addView(itemView);

        return itemView ;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((LinearLayout) object);
    }
}
