package com.cuponexpress.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.cuponexpress.R;
import com.cuponexpress.main.FavDetailsPagerActivity;
import com.cuponexpress.main.MainActivity;

import java.util.ArrayList;

/**
 * Created by ToSuccess on 10/25/2016.
 */

public class GridView_FavoriteAdapter extends BaseAdapter {

    MainActivity _activity ;

    ArrayList<Integer> _img = new ArrayList<>();

    public GridView_FavoriteAdapter (MainActivity activity,ArrayList<Integer> img) {

        this._activity = activity ;
        this._img = img ;
    }

    @Override
    public int getCount() {
        return _img.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        Holder holder ;

        if (convertView == null){

            holder = new Holder();

            LayoutInflater inflater = (LayoutInflater)_activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.item_favorite_gridview , parent, false);

            holder.imvProduct = (ImageView)convertView.findViewById(R.id.imv_products_fav);
            holder.imvProduct.setImageResource(_img.get(position));

            holder.txvTitle = (TextView)convertView.findViewById(R.id.txv_title_fav);
            holder.txvTitle.setText("COCOICON");

            convertView.setTag(holder);

        }else {
            holder =(Holder)convertView.getTag();
        }

        /*convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(_activity, FavDetailsPagerActivity.class);
                intent.putExtra("images", _img);
                intent.putExtra("position",position);
                _activity.startActivity(intent);

                Activity activity = (Activity)_activity ;
                activity.overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);

            }
        });*/

        return convertView ;
    }

    public class Holder {

        public ImageView imvProduct;
        public TextView txvTitle;
    }
}
