package com.cuponexpress.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.cuponexpress.R;
import com.cuponexpress.main.FavDetailsPagerActivity;
import com.cuponexpress.main.MainActivity;

import java.util.ArrayList;

/**
 * Created by ToSuccess on 11/15/2016.
 */

public class FavoViewPagerAdapter extends PagerAdapter{

    ArrayList<Integer> upload_image = new ArrayList<>();
    FavDetailsPagerActivity _activity ;
    LayoutInflater mlayoutInflater;

    public FavoViewPagerAdapter (FavDetailsPagerActivity activity /*, ArrayList<Integer> image*/){

        this._activity = activity ;
        /*this.upload_image = image ;*/
        mlayoutInflater = (LayoutInflater)_activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }
    @Override
    public int getCount() {
        return upload_image.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {

        return view == ((LinearLayout)object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {

        View itemView = mlayoutInflater.inflate(R.layout.fav_pager_item ,container , false);
        ImageView imageView = (ImageView) itemView.findViewById(R.id.item_img_fav);

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Activity activity = (Activity)_activity;
//                _activity.finish();
                activity.overridePendingTransition(R.anim.left_in, R.anim.right_out);
            }
        });

        /*imageView.setImageResource(upload_image.get(position));*/

        container.addView(itemView);

        return itemView ;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((LinearLayout) object);
    }
}
