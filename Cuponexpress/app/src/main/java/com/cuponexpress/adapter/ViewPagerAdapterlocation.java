package com.cuponexpress.adapter;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.cuponexpress.R;
import com.cuponexpress.main.MainActivity;

import java.util.ArrayList;

/**
 * Created by ToSuccess on 10/26/2016.
 */

public class ViewPagerAdapterlocation extends PagerAdapter {

    ArrayList<Integer> imgs = new ArrayList<>();
    ArrayList<Integer> imgs2 = new ArrayList<>();
    LayoutInflater mLayoutInflater;
    MainActivity _activity;

    ImageView ui_imvImg1, ui_imvImg2;
    TextView ui_txvTitle1, ui_txvTiltel2;

    public ViewPagerAdapterlocation(MainActivity activity , ArrayList<Integer> imgs, ArrayList<Integer> imgs2) {

        this._activity = activity ;
        this.imgs = imgs ;
        this.imgs2 = imgs2 ;

        mLayoutInflater = (LayoutInflater)_activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {return imgs.size();}

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view ==((LinearLayout)object) ;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {

        View itemView = mLayoutInflater.inflate(R.layout.item_viewpagerlocation, container, false);

        ui_imvImg1 = (ImageView)itemView.findViewById(R.id.imv_photo_loc);
        ui_imvImg1.setImageResource(imgs.get(position));

        ui_imvImg2 = (ImageView)itemView.findViewById(R.id.imv_photo_loc2);
        ui_imvImg2.setImageResource(imgs2.get(position));

        ui_txvTitle1 = (TextView)itemView.findViewById(R.id.txv_name_loc);
        ui_txvTitle1.setText(R.string.fulgore);

        ui_txvTiltel2 = (TextView)itemView.findViewById(R.id.txv_name2_loc);
        ui_txvTiltel2.setText(R.string.guess);

        container.addView(itemView);

        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((LinearLayout) object);
    }
}
