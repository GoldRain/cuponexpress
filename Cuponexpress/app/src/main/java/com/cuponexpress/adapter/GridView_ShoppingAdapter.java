package com.cuponexpress.adapter;

import android.content.Context;
import android.media.Image;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.cuponexpress.R;
import com.cuponexpress.main.MainActivity;

import java.util.ArrayList;

/**
 * Created by ToSuccess on 11/1/2016.
 */

public class GridView_ShoppingAdapter extends BaseAdapter{

    MainActivity _activity ;
    ArrayList<Integer> _img1 = new ArrayList<>();
    ArrayList<String> _name = new ArrayList<>();

    public GridView_ShoppingAdapter (MainActivity activity, ArrayList<Integer> img1){

        this._activity = activity ;
        this._img1 = img1;

        loadData();
    }

    private void loadData() {

        _img1.add(R.drawable.img1);_img1.add(R.drawable.img5);_img1.add(R.drawable.img3);_img1.add(R.drawable.img7);
        _img1.add(R.drawable.img2);_img1.add(R.drawable.img4);_img1.add(R.drawable.img6);_img1.add(R.drawable.img3);
        _img1.add(R.drawable.img3);_img1.add(R.drawable.img7);_img1.add(R.drawable.img5);_img1.add(R.drawable.img4);
        _img1.add(R.drawable.img4);_img1.add(R.drawable.img8);_img1.add(R.drawable.img4);_img1.add(R.drawable.img8);

    }

    @Override
    public int getCount() {
        return _img1.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ShoppingHolder holder;

        if (convertView == null){

            holder = new ShoppingHolder();

            LayoutInflater inflater = (LayoutInflater)_activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.item_shopping_gridview , parent , false);

            holder.imvProduct = (ImageView)convertView.findViewById(R.id.imv_products_shop);
            holder.imvProduct.setImageResource(_img1.get(position));

            holder.txvName = (TextView)convertView.findViewById(R.id.txv_title_shop);
            holder.txvName.setText("PULL AND BEAR");

            convertView.setTag(holder);

        }else {

            holder = (ShoppingHolder)convertView.getTag();
        }

       /* convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                _activity.showFragment();
            }
        });*/

        return convertView;
    }

    public class ShoppingHolder {

        public ImageView imvProduct;
        public TextView txvName;
    }
}
