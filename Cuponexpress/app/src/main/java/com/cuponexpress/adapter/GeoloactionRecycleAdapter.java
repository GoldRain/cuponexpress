package com.cuponexpress.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.cuponexpress.R;
import com.cuponexpress.main.MainActivity;

import java.util.ArrayList;

/**
 * Created by HugeRain on 3/15/2017.
 */

public class GeoloactionRecycleAdapter extends RecyclerView.Adapter<GeoloactionRecycleAdapter.ImageHolder>{

    MainActivity _activity;
    ArrayList<Integer> _images = new ArrayList<>();
    ArrayList<Integer> _images2 = new ArrayList<>();

    View view ;

    public GeoloactionRecycleAdapter (MainActivity activity , ArrayList<Integer> images){

        this._activity = activity ;
        this._images = images ;
        /*this._images2 = images2 ;*/
        /*_images.add(R.drawable.img2);*/
    }

    @Override
    public GeoloactionRecycleAdapter.ImageHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {

        view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_home_recycl_image,viewGroup, false);
        GeoloactionRecycleAdapter.ImageHolder viewHolder = new GeoloactionRecycleAdapter.ImageHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ImageHolder viewHolder,final int position) {

        Integer imageHolder = _images.get(position);

        viewHolder.imvPhoto.setImageResource(_images.get(position));
        /*viewHolder.imvPhoto2.setImageResource(_images2.get(position));*/
        viewHolder.txvTitle.setText("location");

        viewHolder.imvPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                //_activity.showFragment();

            }
        });

    }


    @Override
    public int getItemCount() {

        return (null!= _images ? _images.size():0);
        /*return _images.size();*/
    }

    public class ImageHolder extends RecyclerView.ViewHolder {

        ImageView imvPhoto ;
        ImageView imvPhoto2;
        TextView txvTitle ;

        public ImageHolder(View view) {
            super(view);
            imvPhoto = (ImageView) view.findViewById(R.id.imv_img_hom);
            /*imvPhoto2 = (ImageView) view.findViewById(R.id.imv_img_hom2);*/
            txvTitle = (TextView)view.findViewById(R.id.txv_name_hom);
        }
    }
}
