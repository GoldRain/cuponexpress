package com.cuponexpress.adapter;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.support.v4.view.PagerAdapter;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.cuponexpress.R;
import com.cuponexpress.logger.Logger;
import com.cuponexpress.main.MainActivity;
import com.ecloud.pulltozoomview.PullToZoomListViewEx;

/**
 * Created by ToSuccess on 12/8/2016.
 */

public class ViewPagerHome_DetailsAdapter extends PagerAdapter {

    MainActivity _activity;
    ListViewHomeDetailsAdapter adapter;

    //ListViewHomeDetailsAdapter _adapter_list;

    private ListView listView;

    ListView ui_lstreviews;

    public ViewPagerHome_DetailsAdapter(MainActivity activity/*,  ArrayList<Integer> imgs,  ArrayList<Integer> imgs2*/)

    {
        this._activity = activity;
    }
    @Override
    public int getCount() {
        return 5;
    }


    @Override
    public Object instantiateItem(ViewGroup container, int position) {

        LayoutInflater mLayoutInflater = (LayoutInflater)_activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View itemView = mLayoutInflater.inflate(R.layout.item_viewpager_details,container, false);

        adapter = new ListViewHomeDetailsAdapter(_activity);
        listView = (ListView) itemView.findViewById(R.id.lst_listview1);
        listView.setAdapter(adapter);

        //LinearLayout ui_lytHeader = (LinearLayout)itemView.findViewById(R.id.lytHeader);

        LayoutInflater myinflater = _activity.getLayoutInflater();
        final ViewGroup myHeader = (ViewGroup)myinflater.inflate(R.layout.list_head_zoom_view, listView, false);
        listView.addHeaderView(myHeader, null, false);

        ImageView imv_coupon = (ImageView)myHeader.findViewById(R.id.imv_canjear_coupon);
        imv_coupon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final Dialog dialog = new Dialog(_activity);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.alert_canjear_coupon);
                dialog.getWindow().setLayout(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(_activity.getResources().getColor(R.color.transparent)));

                TextView txv_si = (TextView)dialog.findViewById(R.id.txv_si);
                txv_si.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Toast.makeText(_activity, "Click si", Toast.LENGTH_SHORT).show();
                    }
                });

                TextView txv_no = (TextView)dialog.findViewById(R.id.txv_no);
                txv_no.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });

                dialog.show();
            }
        });


        ImageView imv_download = (ImageView)myHeader.findViewById(R.id.imv_download_des);
        imv_download.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final Dialog dialog = new Dialog(_activity);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.alert_download);
                dialog.getWindow().setLayout(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(_activity.getResources().getColor(R.color.transparent)));

                ImageView imv_download = (ImageView)dialog.findViewById(R.id.imv_download);
                imv_download.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        Toast.makeText(_activity, "Download...", Toast.LENGTH_SHORT).show();
                    }
                });

                dialog.show();
            }
        });


   /*     ui_lytHeader.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //_activity.detailShowFragment();
            }
        });
*/

        container.addView(itemView);

        return itemView ;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((LinearLayout) object);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((LinearLayout) object);
    }
}
