package com.cuponexpress.adapter;

import android.app.ActionBar;
import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.cuponexpress.R;
import com.cuponexpress.main.MainActivity;

import java.util.ArrayList;

/**
 * Created by ToSuccess on 10/27/2016.
 */

public class ViewPagerAdapterHome extends PagerAdapter {

    MainActivity _activity;
    LayoutInflater mLayoutInflater;
    ArrayList<Integer> imgs = new ArrayList<>();
    ArrayList<Integer> imgs2 = new ArrayList<>();

    ViewPager viewPager;

    ImageView ui_imvImg,ui_imvImg2 ;
    TextView ui_txvTitle, ui_txvName, ui_txvName2;

    public ViewPagerAdapterHome (MainActivity activity,  ArrayList<Integer> imgs,  ArrayList<Integer> imgs2){

        this._activity = activity;
        this.imgs = imgs ;
        this.imgs2 = imgs2 ;

        mLayoutInflater = (LayoutInflater)_activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {return 3;}

    @Override
    public boolean isViewFromObject(View view, Object object) {

        return view == ((LinearLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {

        View itemView = mLayoutInflater.inflate(R.layout.item_viewpagerhome ,container, false);

        ui_imvImg = (ImageView)itemView.findViewById(R.id.imv_img_hom);
        ui_imvImg.setImageResource(imgs.get(position));

        ui_imvImg2 = (ImageView)itemView.findViewById(R.id.imv_img2_hom);
        ui_imvImg2.setImageResource(imgs2.get(position));


        ui_txvName = (TextView)itemView.findViewById(R.id.txv_name_hom);
        ui_txvName.setText(R.string.guess);

        ui_txvName2 = (TextView)itemView.findViewById(R.id.txv_name2_hom);
        ui_txvName2.setText(R.string.fulgore);

        container.addView(itemView);

        return itemView ;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((LinearLayout) object);
    }

}
