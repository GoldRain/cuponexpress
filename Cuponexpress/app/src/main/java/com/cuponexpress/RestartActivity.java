package com.cuponexpress;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.KeyEvent;
import android.view.View;

import com.cuponexpress.base.CommonActivity;
import com.cuponexpress.commons.Commons;
import com.cuponexpress.main.SignInActivity;

/**
 * Created by ToSuccess on 10/23/2016.
 */

public class RestartActivity extends CommonActivity implements View.OnClickListener {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (!Commons.g_isAppRunning) {

            Intent goMain = new Intent(this, SignInActivity.class);
            startActivity(goMain);
        }

        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        return true;
    }

    @Override
    public void onClick(View view) {

    }
}
