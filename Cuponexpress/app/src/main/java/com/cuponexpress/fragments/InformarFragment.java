package com.cuponexpress.fragments;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.cuponexpress.CuponExpressApplication;
import com.cuponexpress.R;
import com.cuponexpress.base.BaseFragment;
import com.cuponexpress.commons.Commons;
import com.cuponexpress.commons.Constants;
import com.cuponexpress.commons.ReqConst;
import com.cuponexpress.main.MainActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;


public class InformarFragment extends BaseFragment implements View.OnClickListener{

    MainActivity _activity;
    View view ;
    ImageView ui_imvBack ;
    EditText ui_edtContent ;
    TextView informar;

    public InformarFragment(MainActivity activity) { this._activity = activity; }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_informar, container, false);

        loadLayout();

        return view ;
    }

    private void loadLayout() {

        ui_imvBack = (ImageView)_activity.findViewById(R.id.imv_back);
        ui_imvBack.setOnClickListener(this);

        ui_edtContent = (EditText)view.findViewById(R.id.edt_content_inf);

        informar=(TextView)view.findViewById(R.id.txv_btn_inf);
        informar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkValue()){
                    uploadProblem();
                }
            }
        });

        RelativeLayout linearLayout = (RelativeLayout)view.findViewById(R.id.lyt_container);
        linearLayout.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {

                //TODO Auto-generated method stub
                InputMethodManager imm = (InputMethodManager)_activity.getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(ui_edtContent.getWindowToken(),0);
                return false;

                // STOPSHIP: 11/27/2016 :
            }
        });

    }

    public boolean checkValue(){

        if (ui_edtContent.getText().toString().length() == 0){
            _activity.showAlertDialog(getString(R.string.enter_content));
            return false;
        }
        return true ;
    }

    private void uploadProblem() {

        _activity.showProgress();
        String url = ReqConst.SERVER_URL + ReqConst.REQ_REPORT;

        StringRequest myRequest = new StringRequest(
                Request.Method.POST, url ,new Response.Listener<String>() {

                    @Override
                    public void onResponse(String response) {

                        Log.d("response=====", String.valueOf(response));
                        parseRegisterResponse(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("error============", String.valueOf(error));
                        _activity.closeProgress();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

               try {
                   params.put("id", String.valueOf(Commons.g_user.get_idx()));
                   params.put("problem", ui_edtContent.getText().toString());

               } catch (Exception e) {}
                return params;
            }
        };

        myRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        CuponExpressApplication.getInstance().addToRequestQueue(myRequest,url);  // url or "tag"
    }

    private void parseRegisterResponse(String response) {

        try {

            JSONObject object = new JSONObject(response);

            int result_code = object.getInt(ReqConst.RES_CODE);

            if (result_code == ReqConst.CODE_SUCCESS) {

                Log.d("result_code=========", String.valueOf(result_code));

               _activity.configFragment();

                Toast.makeText(_activity, "Upload Success", Toast.LENGTH_SHORT).show();
                _activity.closeProgress();

            }else if(result_code == ReqConst.CODE_UNKNOWNERRO){}else {

                _activity.showToast("Unknown Error. Please Try Again.");
                _activity.closeProgress();
            }

        } catch (JSONException e) {
            e.printStackTrace();
            showToast("Network Error. Please Try Again.");
            _activity.closeProgress();
        }
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){
            case R.id.imv_back:
                _activity.configFragment();
        }
    }

    @Override
    public void onResume() {
        _activity.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        super.onResume();
    }

    @Override
    public void onStart() {
        _activity.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        super.onStart();
    }
}
