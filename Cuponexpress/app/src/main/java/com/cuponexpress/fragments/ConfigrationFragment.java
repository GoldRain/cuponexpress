package com.cuponexpress.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.cuponexpress.R;
import com.cuponexpress.adapter.ListView_ConfigAdapter;
import com.cuponexpress.base.BaseFragment;
import com.cuponexpress.main.MainActivity;


public class ConfigrationFragment extends BaseFragment implements View.OnClickListener{

    MainActivity _activity;
    View view;
    ImageView ui_imvBack ;
    TextView ui_txvTitle ;

    LinearLayout  ui_lyt_location, ui_lyt_home, ui_lyt_favo, ui_lyt_conf;
    RelativeLayout  ui_ryt_main;
    RelativeLayout ui_ryt_conf;

    LinearLayout ui_lytRegi, ui_lytCamb, ui_lytTec, ui_lytInfor, ui_lytAyuda;

    public ConfigrationFragment(MainActivity activity){ this._activity = activity ;}


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_configration, container, false);


        loadLayout();
        // Inflate the layout for this fragment

        keyHide();
        return view;
    }

    public void keyHide(){

        //TODO Auto-generated method stub
        InputMethodManager imm = (InputMethodManager)_activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(_activity.getCurrentFocus().getWindowToken(),InputMethodManager.HIDE_NOT_ALWAYS);
    }

    private void loadLayout() {

        ui_imvBack = (ImageView)_activity.findViewById(R.id.imv_back);
        ui_imvBack.setOnClickListener(this);

        ui_txvTitle = (TextView)_activity.findViewById(R.id.txv_title_con);
        ui_txvTitle.setText(getText(R.string.configuracion));

        ui_lytRegi = (LinearLayout)view.findViewById(R.id.lyt_registrar_con);
        ui_lytRegi.setOnClickListener(this);

        ui_lytCamb = (LinearLayout)view.findViewById(R.id.lyt_cambiar_con);
        ui_lytCamb.setOnClickListener(this);

        ui_lytTec = (LinearLayout)view.findViewById(R.id.lyt_tec_con);
        ui_lytTec.setOnClickListener(this);

        ui_lytInfor = (LinearLayout)view.findViewById(R.id.lyt_informar_con);
        ui_lytInfor.setOnClickListener(this);

        ui_lytAyuda = (LinearLayout)view.findViewById(R.id.lyt_ayuda_con);
        ui_lytAyuda.setOnClickListener(this);

        ui_lyt_location = (LinearLayout)_activity.findViewById(R.id.lyt_location);
        ui_lyt_location.setOnClickListener(this);

        ui_lyt_home = (LinearLayout)_activity.findViewById(R.id.lyt_home);
        ui_lyt_home.setOnClickListener(this);

        ui_lyt_favo = (LinearLayout)_activity.findViewById(R.id.lyt_favo);
        ui_lyt_favo.setOnClickListener(this);

        ui_ryt_main = (RelativeLayout)_activity.findViewById(R.id.ryt_main);
        ui_ryt_conf = (RelativeLayout)_activity.findViewById(R.id.ryt_config);

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){

            case R.id.imv_back:
                _activity.homeFragment();
                _activity.titleBarChange(View.VISIBLE, View.GONE, View.GONE,View.GONE,View.GONE);
                break;

            case R.id.lyt_home:
                _activity.homeFragment();
                _activity.titleBarChange(View.VISIBLE, View.GONE, View.GONE ,View.GONE,View.GONE);
                break;

            case R.id.lyt_location:
                _activity.geolocationFragment();
                _activity.titleBarChange(View.VISIBLE, View.GONE, View.GONE,View.GONE,View.GONE);
                break;

            case R.id.lyt_favo:
                _activity.favoritsFragment();
                _activity.titleBarChange(View.VISIBLE, View.GONE, View.GONE,View.GONE,View.GONE);
                break;

            case R.id.lyt_registrar_con:

                _activity.registrarFragment();
                ui_txvTitle.setText(getText(R.string.registrar));
                break;

            case R.id.lyt_cambiar_con:

                ui_txvTitle.setText(getText(R.string.cambiar));
                _activity.cambiarFragment();
                break;

            case R.id.lyt_tec_con:
                ui_txvTitle.setText(getText(R.string.tecnolocia));
                _activity.terminosFragment();
                break;

            case R.id.lyt_informar_con:
                ui_txvTitle.setText(getText(R.string.informar));
                _activity.informarFragment();
                break;

            case R.id.lyt_ayuda_con:
                ui_txvTitle.setText(getText(R.string.ayuda));
                _activity.ayudaFragment();
                break;
        }

    }


}
