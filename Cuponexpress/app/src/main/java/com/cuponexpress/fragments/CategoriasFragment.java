package com.cuponexpress.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;

import com.cuponexpress.R;
import com.cuponexpress.adapter.GridView_CategoriesAdapter;
import com.cuponexpress.main.MainActivity;

import java.util.ArrayList;

public class CategoriasFragment extends Fragment {

    MainActivity _activity ;
    GridView_CategoriesAdapter categories_adapter;
    GridView ui_gridView_cat;

    ArrayList<Integer> image_cat = new ArrayList<>();
    ArrayList<String> name = new ArrayList<>();
    View view ;

    public CategoriasFragment(MainActivity activity) {

        this._activity = activity ;
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_categorias, container, false);

        loadLayout();
        loadImage();
        loadName();

        return view ;
    }

    private void loadImage() {

        image_cat.add(R.drawable.cat_gastronomia);
        image_cat.add(R.drawable.cat_beauty);
        image_cat.add(R.drawable.aeroplane);
        image_cat.add(R.drawable.cat_sports);
        image_cat.add(R.drawable.cat_entertainment);
        image_cat.add(R.drawable.cat_shopping);
        image_cat.add(R.drawable.cat_vehicle);
        image_cat.add(R.drawable.cat_home);
        image_cat.add(R.drawable.cat_course);
        image_cat.add(R.drawable.cat_technology);
    }

    private void loadName() {

        name.add("Gastronomy");
        name.add("Belleza");
        name.add("Turismo");
        name.add("Deporte");
        name.add("Entretenimiento");
        name.add("Shopping");
        name.add("Vehiculos");
        name.add("Horgar");
        name.add("CursoseInfo");
        name.add("Tecnologia");
    }

    private void loadLayout() {

        ui_gridView_cat = (GridView)view.findViewById(R.id.grv_container_cat);
        categories_adapter = new GridView_CategoriesAdapter(_activity,image_cat, name);
        ui_gridView_cat.setAdapter(categories_adapter);

        ui_gridView_cat.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                switch (position){
                    case 0:
                        _activity.gastronoFragment();
                        break;

                    case 1:

                        _activity.bellizaFragment();
                        break;
                    case 2:

                        _activity.turismoFragment();
                        break;
                    case 3:

                        _activity.deporteFragment();
                        break;

                    case 4:
                        _activity.entretenFragment();
                        break;
                    case 5:
                        _activity.shoppingFragment();
                        break;

                    case 6:
                        _activity.vehiculosFragment();
                        break;
                    case 7:
                        _activity.horgarFragment();
                        break;
                    case 8:

                        _activity.cursosFragment();
                        break;
                    case 9:
                        _activity.tecnologFragment();
                        break;
                }
            }
        });
    }



}
