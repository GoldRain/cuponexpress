package com.cuponexpress.fragments;

import android.app.Dialog;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.cuponexpress.R;
import com.cuponexpress.adapter.ListViewHomeDetailsAdapter;
import com.cuponexpress.adapter.ViewPagerHome_DetailsAdapter;
import com.cuponexpress.main.MainActivity;
import com.iarcuschin.simpleratingbar.SimpleRatingBar;


public class ShowFragment extends Fragment implements View.OnClickListener {

    MainActivity _activity ;
    View view ;

    ImageView ui_imvShow;
    ImageView ui_imvBack;
    TextView ui_txvTitle;

    ListViewHomeDetailsAdapter _adapter_home_ ;

    ViewPagerHome_DetailsAdapter _adapter_home_show;
   ViewPager ui_viewpager;

    int state =0 ;
    int cur_rating = 0;

    public ShowFragment(MainActivity activity) { this._activity = activity ;}

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_show, container, false);

        loadLayout();

        return view ;
    }

    public void loadLayout() {

        /*REVIEW ALERT*/
        ImageView imv_review = (ImageView)_activity.findViewById(R.id.imv_review);
        imv_review.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final Dialog dialog = new Dialog(_activity);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.alert_rating);
                dialog.getWindow().setLayout(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(_activity.getResources().getColor(R.color.transparent)));

                TextView txv_publicar = (TextView) dialog.findViewById(R.id.txv_publicar);
                txv_publicar.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        Toast.makeText(_activity, "Review!", Toast.LENGTH_SHORT).show();

                        dialog.dismiss();
                    }
                });


                EditText edt_review = (EditText) dialog.findViewById(R.id.edt_reviw_content);

                SimpleRatingBar srb_review;
                srb_review = (SimpleRatingBar)dialog.findViewById(R.id.srb_review);
                srb_review.setOnRatingBarChangeListener(new SimpleRatingBar.OnRatingBarChangeListener() {
                    @Override
                    public void onRatingChanged(SimpleRatingBar simpleRatingBar, float rating, boolean fromUser) {
                        cur_rating = (int)rating;
                        _activity.showToast(String.valueOf(cur_rating));
                    }
                });

                dialog.show();

            }
        });

        ui_viewpager = (ViewPager) view.findViewById(R.id.vpg_show);
        _adapter_home_show = new ViewPagerHome_DetailsAdapter(_activity);
        ui_viewpager.setAdapter(_adapter_home_show);

       /* ui_imvShow = (ImageView)view.findViewById(R.id.imv_show);
        ui_imvShow.setOnClickListener(this);
*/
        ui_imvBack = (ImageView)_activity.findViewById(R.id.imv_back_show);
        ui_imvBack.setOnClickListener(this);

        ui_txvTitle = (TextView)_activity.findViewById(R.id.txv_title_con);
        ui_txvTitle.setText("");
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){

            case R.id.imv_back_show:
//
           /*     if ( Commons.status == Constants.CUPONES){

                    _activity.cuponesFragment();

                    Log.d(">cupones+++++++++++",String.valueOf(Commons.status));

                    Commons.status = Constants.CUPONES ;



                }else if ( Commons.status == Constants.SHOPPING ){

                    Log.d("shopping============>>>",String.valueOf(Commons.status));

                    Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            _activity.shoppingFragment();
                        }

                    },100);

                    Commons.status = Constants.SHOPPING ;


                } else if(Commons.status == Constants.FAVORITE){

                    _activity.favoritsFragment();
                    Log.d("favo=====<<<<+++++++", String.valueOf(Commons.status));
                    Commons.status = Constants.FAVORITE ;

                } else {

                    _activity.showToast("faild");
                }*/

                _activity.homeFragment();

                _activity.overridePendingTransition(0,0);

                break;
        }
    }
}
