package com.cuponexpress.fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.ImageView;

import com.cuponexpress.R;
import com.cuponexpress.adapter.GridView_CommonsAdapter;
import com.cuponexpress.main.MainActivity;

import java.util.ArrayList;


public class TurismoFragment extends Fragment {

    MainActivity _activity ;
    View view ;

    GridView_CommonsAdapter turismo_adapter;
    ArrayList<Integer> img_turismo = new ArrayList<>();
    GridView ui_gridView_turismo;

    ImageView ui_imvBack ;


    public TurismoFragment(MainActivity activity) {
        // Required empty public constructor
        this._activity = activity ;

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_turismo, container, false);

        loadLayout();
        loadDate();

        return view ;
    }

    private void loadDate() {

        img_turismo.add(R.drawable.img5); img_turismo.add(R.drawable.img6); img_turismo.add(R.drawable.img8); img_turismo.add(R.drawable.img1);
        img_turismo.add(R.drawable.img4); img_turismo.add(R.drawable.img5); img_turismo.add(R.drawable.img3); img_turismo.add(R.drawable.img7);
        img_turismo.add(R.drawable.img3); img_turismo.add(R.drawable.img4); img_turismo.add(R.drawable.img1); img_turismo.add(R.drawable.img8);
        img_turismo.add(R.drawable.img1); img_turismo.add(R.drawable.img3); img_turismo.add(R.drawable.img5); img_turismo.add(R.drawable.img2);
    }

    public void loadLayout() {

        ui_imvBack = (ImageView)_activity.findViewById(R.id.imv_back_cat);
        ui_imvBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                _activity.categoryFragment();
            }
        });

        turismo_adapter = new GridView_CommonsAdapter(_activity,img_turismo);
        ui_gridView_turismo = (GridView)view.findViewById(R.id.grd_turismo);
        ui_gridView_turismo.setAdapter(turismo_adapter);
    }

}
