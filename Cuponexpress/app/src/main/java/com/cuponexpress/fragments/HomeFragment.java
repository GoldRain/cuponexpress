package com.cuponexpress.fragments;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.cuponexpress.R;
import com.cuponexpress.adapter.HomeRecyclerViewAdapter;
import com.cuponexpress.adapter.ListView_HomeAdapter;
import com.cuponexpress.base.BaseFragment;
import com.cuponexpress.main.MainActivity;

import java.util.ArrayList;


public class HomeFragment extends BaseFragment implements View.OnClickListener {

    ListView_HomeAdapter adapter;
    MainActivity activity;

    ArrayList<Integer> imgs = new ArrayList<>();
    ArrayList<Integer> imgs2 = new ArrayList<>();
    ArrayList<String> title = new ArrayList<>();

    View view ;
    ListView ui_lst_home;

    RecyclerView ui_recyclerImage ;
    HomeRecyclerViewAdapter _recyclerViewAdapter;
    ArrayList<Integer> _images = new ArrayList<>();

    public HomeFragment (MainActivity activity){
        this.activity = activity;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_home, container, false);

        loadLayout();

        loadTitle();

        loadData();

        return view;
    }

    public void loadTitle() {

        title.add("DESTACADOS");
        title.add("RECOMENDADOS");
        title.add("MAS DESCARGADOS");
    }
    private void loadData() {

    /*    imgs.add(R.drawable.img1);
        imgs2.add(R.drawable.img2);

        imgs.add(R.drawable.img3);
        imgs2.add(R.drawable.img4);

        imgs.add(R.drawable.img5);
        imgs2.add(R.drawable.img6);*/
    }

    private void loadLayout() {

        adapter = new ListView_HomeAdapter(activity,imgs ,title);
        ui_lst_home = (ListView)view.findViewById(R.id.lst_contaner);
        ui_lst_home.setAdapter(adapter);

     /*   ui_recyclerImage = (RecyclerView)view.findViewById(R.id.rev_image);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false);
        ui_recyclerImage.setLayoutManager(linearLayoutManager);

        _recyclerViewAdapter = new HomeRecyclerViewAdapter(activity, imgs);
        ui_recyclerImage.setAdapter(_recyclerViewAdapter);*/
    }

    @Override
    public void onClick(View view) {

    }

}
