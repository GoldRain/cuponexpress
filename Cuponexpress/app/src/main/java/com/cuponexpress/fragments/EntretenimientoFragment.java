package com.cuponexpress.fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.ImageView;

import com.cuponexpress.R;
import com.cuponexpress.adapter.GridView_CommonsAdapter;
import com.cuponexpress.base.BaseFragment;
import com.cuponexpress.main.MainActivity;

import java.util.ArrayList;


public class EntretenimientoFragment extends BaseFragment {

    MainActivity _activity;
    View view ;

    GridView_CommonsAdapter entre_adapter;
    ArrayList<Integer> img_entre = new ArrayList<>();
    GridView ui_gridView_entre;

    ImageView ui_imvBack ;

    public EntretenimientoFragment(MainActivity activity) {
        // Required empty public constructor

        this._activity =  activity ;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_entretenimiento, container, false);

        loadLayout();
        loadDate();

        return view ;
    }
    private void loadDate() {

        img_entre.add(R.drawable.img5); img_entre.add(R.drawable.img6); img_entre.add(R.drawable.img8); img_entre.add(R.drawable.img1);
        img_entre.add(R.drawable.img4); img_entre.add(R.drawable.img5); img_entre.add(R.drawable.img3); img_entre.add(R.drawable.img7);
        img_entre.add(R.drawable.img3); img_entre.add(R.drawable.img4); img_entre.add(R.drawable.img1); img_entre.add(R.drawable.img8);
        img_entre.add(R.drawable.img1); img_entre.add(R.drawable.img3); img_entre.add(R.drawable.img5); img_entre.add(R.drawable.img2);
    }

    private void loadLayout() {

        ui_imvBack = (ImageView)_activity.findViewById(R.id.imv_back_cat);
        ui_imvBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                _activity.categoryFragment();
            }
        });

        entre_adapter = new GridView_CommonsAdapter(_activity,img_entre);
        ui_gridView_entre = (GridView)view.findViewById(R.id.grd_entret);
        ui_gridView_entre.setAdapter(entre_adapter);
    }
}
