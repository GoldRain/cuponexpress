package com.cuponexpress.fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import com.cuponexpress.R;
import com.cuponexpress.adapter.GridView_CommonsAdapter;
import com.cuponexpress.main.MainActivity;

import java.util.ArrayList;


public class GastronFragment extends Fragment {

    MainActivity _activity;
    GridView_CommonsAdapter gastron_adapter;
    ArrayList<Integer> img_gas = new ArrayList<>();
    View view ;

    ImageView ui_imvBack ;
    GridView ui_gridView_gas;

    public GastronFragment(MainActivity activity) {
        // Required empty public constructor
       this._activity = activity ;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_gastron, container, false);

        loadDate();
        loadLayout();
        return view ;

    }

    private void loadDate() {

        img_gas.add(R.drawable.img5); img_gas.add(R.drawable.img6); img_gas.add(R.drawable.img8); img_gas.add(R.drawable.img1);
        img_gas.add(R.drawable.img4); img_gas.add(R.drawable.img5); img_gas.add(R.drawable.img3); img_gas.add(R.drawable.img7);
        img_gas.add(R.drawable.img3); img_gas.add(R.drawable.img4); img_gas.add(R.drawable.img1); img_gas.add(R.drawable.img8);
        img_gas.add(R.drawable.img1); img_gas.add(R.drawable.img3); img_gas.add(R.drawable.img5); img_gas.add(R.drawable.img2);

    }

    public void loadLayout() {

        ui_imvBack = (ImageView)_activity.findViewById(R.id.imv_back_cat);
        ui_imvBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                _activity.categoryFragment();
            }
        });

        gastron_adapter = new GridView_CommonsAdapter(_activity,img_gas);
        ui_gridView_gas = (GridView)view.findViewById(R.id.grd_gastro);
        ui_gridView_gas.setAdapter(gastron_adapter);
    }



}

