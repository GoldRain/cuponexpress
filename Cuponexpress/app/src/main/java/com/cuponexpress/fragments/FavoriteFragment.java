package com.cuponexpress.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;

import com.cuponexpress.R;
import com.cuponexpress.adapter.GridView_CommonsAdapter;
import com.cuponexpress.adapter.GridView_FavoriteAdapter;
import com.cuponexpress.base.BaseFragment;
import com.cuponexpress.main.MainActivity;

import java.lang.reflect.Array;
import java.util.ArrayList;


public class FavoriteFragment extends BaseFragment {


    GridView_FavoriteAdapter favor_adapter;
    MainActivity _activity;
    ArrayList<Integer> img_fav = new ArrayList<>();
    View view;

    GridView ui_grv_view;

    public FavoriteFragment (MainActivity activity){this._activity = activity ;}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_favorite, container, false);

        loadLayout();

        loadData();

        return view;
    }

    private void loadData() {

        img_fav.add(R.drawable.img_c1); img_fav.add(R.drawable.img_c2); img_fav.add(R.drawable.img_c3); img_fav.add(R.drawable.img_c4);
        img_fav.add(R.drawable.img_c5); img_fav.add(R.drawable.img_c6); img_fav.add(R.drawable.img_c7); img_fav.add(R.drawable.img_c8);
        img_fav.add(R.drawable.img_c9); img_fav.add(R.drawable.img_c2); img_fav.add(R.drawable.img_c3); img_fav.add(R.drawable.img_c4);
        img_fav.add(R.drawable.img_c5); img_fav.add(R.drawable.img_c6); img_fav.add(R.drawable.img_c7); img_fav.add(R.drawable.img_c8);
    }

    private void loadLayout() {

        favor_adapter = new GridView_FavoriteAdapter(_activity, img_fav);
        ui_grv_view = (GridView)view.findViewById(R.id.grd_container);
        ui_grv_view.setAdapter(favor_adapter);

    }
}
