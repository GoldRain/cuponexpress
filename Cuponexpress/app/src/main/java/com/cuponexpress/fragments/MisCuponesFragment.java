package com.cuponexpress.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;

import com.cuponexpress.R;
import com.cuponexpress.adapter.GridView_CommonsAdapter;
import com.cuponexpress.base.BaseFragment;
import com.cuponexpress.main.MainActivity;

import java.util.ArrayList;

public class MisCuponesFragment extends BaseFragment {

    MainActivity _activity ;
    View view ;

    GridView_CommonsAdapter miscupon_adapter;
    ArrayList<Integer> img_cup = new ArrayList<>();
    GridView ui_gridView_cup;

    public MisCuponesFragment (MainActivity activity){

        this._activity = activity ;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_mis_cupones, container, false);

        loadLayout();
        loadDate();

        return view ;
    }

    private void loadDate() {

        img_cup.add(R.drawable.img_c1); img_cup.add(R.drawable.img_c2); img_cup.add(R.drawable.img_c3); img_cup.add(R.drawable.img_c4);
        img_cup.add(R.drawable.img_c5); img_cup.add(R.drawable.img_c6); img_cup.add(R.drawable.img_c7); img_cup.add(R.drawable.img_c8);
        img_cup.add(R.drawable.img_c9); img_cup.add(R.drawable.img_c2); img_cup.add(R.drawable.img_c3); img_cup.add(R.drawable.img_c4);
        img_cup.add(R.drawable.img_c5); img_cup.add(R.drawable.img_c6); img_cup.add(R.drawable.img_c7); img_cup.add(R.drawable.img_c8);
    }

    private void loadLayout() {

        miscupon_adapter = new GridView_CommonsAdapter(_activity,img_cup);
        ui_gridView_cup = (GridView)view.findViewById(R.id.grd_container);
        ui_gridView_cup.setAdapter(miscupon_adapter);
    }

}
