package com.cuponexpress.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.ImageView;

import com.cuponexpress.R;
import com.cuponexpress.adapter.GridView_CommonsAdapter;
import com.cuponexpress.adapter.GridView_ShoppingAdapter;
import com.cuponexpress.main.MainActivity;

import java.util.ArrayList;


public class ShoppingFragment extends Fragment {

    MainActivity _activity ;

    ArrayList<Integer> img1_shop = new ArrayList<>();
    GridView_ShoppingAdapter shopping_adapter;
    View view ;
    GridView ui_gridView;

    ImageView ui_imvBack ;

    public ShoppingFragment(MainActivity activity) {this._activity = activity ;}

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_shopping, container, false);

        loadLayout();
        return view ;
    }

    private void loadLayout() {

        ui_imvBack = (ImageView)_activity.findViewById(R.id.imv_back_cat);
        ui_imvBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                _activity.categoryFragment();
            }
        });

        shopping_adapter = new GridView_ShoppingAdapter(_activity,img1_shop );
        ui_gridView = (GridView)view.findViewById(R.id.grd_container);
        ui_gridView.setAdapter(shopping_adapter);

    }

}
