package com.cuponexpress.fragments;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.cuponexpress.R;
import com.cuponexpress.adapter.GeoloactionRecycleAdapter;
import com.cuponexpress.adapter.HomeRecyclerViewAdapter;
import com.cuponexpress.adapter.ViewPagerAdapterlocation;
import com.cuponexpress.base.BaseFragment;
import com.cuponexpress.main.MainActivity;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.Date;


/**
 * Created by ToSuccess on 10/24/2016.
 */

public class GeolocationFragment extends BaseFragment implements View.OnClickListener, OnMapReadyCallback {

    MainActivity _activity ;
    ViewPagerAdapterlocation adapterLocation ;
    ArrayList<Integer> imgs = new ArrayList<>();
    ArrayList<Integer> imgs2 = new ArrayList<>();

    RecyclerView ui_Grecycler ;
    GeoloactionRecycleAdapter _grecy_adapter;
    View view;

    public LocationManager myLocationManager;
    public boolean w_bGpsEnabled, w_bNetworkEnabled;

    public static double w_fLatitude = 0;
    public static double w_fLongitude = 0;
    public static double w_fspeed = 0;

    GoogleMap mMap ;

    /*/////////////////////////START////////////////////////////////*/

    public GeolocationFragment(MainActivity activity){
          this._activity = activity;
      }

    @Override
    public View onCreateView(LayoutInflater inflater,  ViewGroup container, Bundle savedInstanceState) {

      view = inflater.inflate(R.layout.fragment_geolocation, container, false);

        if (mMap == null) {

            SupportMapFragment mapFrag = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map5);
            mapFrag.getMapAsync(this);
        }

        loadData();
        loadLayout();

        return view;
    }

    private void loadData() {

        imgs.add(R.drawable.img_c1); imgs.add(R.drawable.img_c2); imgs.add(R.drawable.img_c3); imgs.add(R.drawable.img_c4);
        imgs.add(R.drawable.img_c5); imgs.add(R.drawable.img_c6); imgs.add(R.drawable.img_c7); imgs.add(R.drawable.img_c8);
        imgs.add(R.drawable.img_c9); imgs.add(R.drawable.img_c2); imgs.add(R.drawable.img_c3); imgs.add(R.drawable.img_c4);
    }

    private void loadLayout() {

        ui_Grecycler = (RecyclerView)view.findViewById(R.id.rev_geo);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(_activity, LinearLayoutManager.HORIZONTAL, false);
        ui_Grecycler.setLayoutManager(linearLayoutManager);
        _grecy_adapter = new GeoloactionRecycleAdapter(_activity,imgs);
        ui_Grecycler.setAdapter(_grecy_adapter);

    }

    @Override
    public void onClick(View view){

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        _activity = (MainActivity)context;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        mMap = googleMap ;
        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);

      //  mMap.setMyLocationEnabled(true);
        initLocationListener();

    }
    public void initLocationListener() {

        myLocationManager = (LocationManager) _activity.getSystemService(Context.LOCATION_SERVICE);
        boolean w_bGpsEnabled = myLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        boolean w_bNetworkEnabled = myLocationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

        if (w_bGpsEnabled || w_bNetworkEnabled) {
            tryGetLocation();

        } else {

            setMyLocation(null);
        }

    }


    private void tryGetLocation() {


        w_bGpsEnabled = myLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        w_bNetworkEnabled = myLocationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

        if (ActivityCompat.checkSelfPermission(_activity, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(_activity, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }

        if (w_bNetworkEnabled) {

            Location locationNet = myLocationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
            if (locationNet != null) {
                setMyLocation(locationNet);
            }

        }
        if (w_bGpsEnabled) {

            Location locationGps = myLocationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            if (locationGps != null) {
                setMyLocation(locationGps);
            }
        }

        if (w_bNetworkEnabled) {

            myLocationManager.requestLocationUpdates(
                    LocationManager.NETWORK_PROVIDER,
                    1000,
                    0, m_myLocationListener);

        }
        if (w_bGpsEnabled) {
            myLocationManager.requestLocationUpdates(
                    LocationManager.GPS_PROVIDER,
                    1000,
                    0, m_myLocationListener);
        }
    }

    private void setMyLocation(Location p_location) {

        if (p_location != null) {
            w_fLatitude = p_location.getLatitude();
            w_fLongitude = p_location.getLongitude();
            w_fspeed=p_location.getSpeed();
            LatLng latLng=new LatLng(w_fLatitude,w_fLongitude);
            CameraUpdate yourLocation = CameraUpdateFactory.newLatLngZoom(latLng, 10);
            mMap.moveCamera(yourLocation);
            mMap.addMarker(new MarkerOptions().position(latLng).title("You are here!").snippet("Consider yourself located"));


        }

        System.out.println("Latitude: " + String.valueOf(w_fLatitude) + "Longitude: " + String.valueOf(w_fLongitude)+"Speed"+ String.valueOf(w_fspeed));

    }


    LocationListener m_myLocationListener = new LocationListener() {

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {
        }

        @Override
        public void onProviderEnabled(String provider) {
        }

        @Override
        public void onProviderDisabled(String provider) {
        }


        @Override
        public void onLocationChanged(Location location) {

            String log = "pos = " + location.getLatitude() + "," + location.getLongitude() + "," + location.getSpeed() + ":" + new Date().toString();
            // appendLog(log);
            setMyLocation(location);
        }
    };

}
