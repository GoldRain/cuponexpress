package com.cuponexpress.fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.ImageView;

import com.cuponexpress.R;
import com.cuponexpress.adapter.GridView_CommonsAdapter;
import com.cuponexpress.base.BaseFragment;
import com.cuponexpress.main.MainActivity;

import java.util.ArrayList;


public class TecnologiaFragment extends BaseFragment {

    MainActivity _activity ;
    View view ;
    GridView_CommonsAdapter tecno_adapter;
    ArrayList<Integer> img_tecno = new ArrayList<>();
    GridView ui_gridView_tecno;

    ImageView ui_imvBack ;

    public TecnologiaFragment(MainActivity activity) {
        // Required empty public constructor

        this._activity = activity ;
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_tecnologia, container, false);

        loadLayout();
        loadDate();

        return view ;

    }
    private void loadDate() {

        img_tecno.add(R.drawable.img5); img_tecno.add(R.drawable.img6); img_tecno.add(R.drawable.img8); img_tecno.add(R.drawable.img1);
        img_tecno.add(R.drawable.img4); img_tecno.add(R.drawable.img5); img_tecno.add(R.drawable.img3); img_tecno.add(R.drawable.img7);
        img_tecno.add(R.drawable.img3); img_tecno.add(R.drawable.img4); img_tecno.add(R.drawable.img1); img_tecno.add(R.drawable.img8);
        img_tecno.add(R.drawable.img1); img_tecno.add(R.drawable.img3); img_tecno.add(R.drawable.img5); img_tecno.add(R.drawable.img2);
    }

    private void loadLayout() {

        ui_imvBack = (ImageView)_activity.findViewById(R.id.imv_back_cat);
        ui_imvBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                _activity.categoryFragment();
            }
        });

        tecno_adapter = new GridView_CommonsAdapter(_activity,img_tecno);
        ui_gridView_tecno = (GridView)view.findViewById(R.id.grd_tecnologia);
        ui_gridView_tecno.setAdapter(tecno_adapter);
    }


}
