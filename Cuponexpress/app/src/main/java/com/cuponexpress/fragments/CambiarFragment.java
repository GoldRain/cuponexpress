package com.cuponexpress.fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.cuponexpress.CuponExpressApplication;
import com.cuponexpress.R;
import com.cuponexpress.base.BaseFragment;
import com.cuponexpress.commons.Commons;
import com.cuponexpress.commons.Constants;
import com.cuponexpress.commons.ReqConst;
import com.cuponexpress.main.MainActivity;
import com.cuponexpress.model.UserEntity;
import com.cuponexpress.preference.PrefConst;
import com.cuponexpress.preference.Preference;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

public class CambiarFragment extends BaseFragment implements View.OnClickListener{

    MainActivity _activity ;
    View view;
    String user_password = null ;


    UserEntity _user = new UserEntity();

    ImageView ui_imvBack;
    TextView ui_txvButton ;

    EditText ui_edtActual, ui_edtNueva, ui_edtConta;

    public CambiarFragment(MainActivity activity) { this._activity = activity;}

    @Override
    public void onCreate(Bundle savedInstanceState) { super.onCreate(savedInstanceState); }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_cambiar, container, false);
        // Inflate the layout for this fragment

        loadLayout();
        return view ;
    }

    private void loadLayout() {

        ui_imvBack = (ImageView)_activity.findViewById(R.id.imv_back);
        ui_imvBack.setOnClickListener(this);

        ui_edtActual = (EditText) view.findViewById(R.id.edt_actual_cam);
        ui_edtNueva = (EditText)view.findViewById(R.id.edt_nueva_cam);
        ui_edtConta = (EditText)view.findViewById(R.id.edt_contra_cam);

        ui_txvButton = (TextView)view.findViewById(R.id.txv_cambiar_cam);
        ui_txvButton.setOnClickListener(this);

        LinearLayout linearLayout = (LinearLayout)view.findViewById(R.id.lyt_container);
        linearLayout.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {

                //TODO Auto-generated method stub
                InputMethodManager imm = (InputMethodManager)_activity.getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(ui_edtActual.getWindowToken(),0);
                return false;
            }
        });

    }

    public boolean checkValue(){

         user_password = Commons.g_user.get_password();

        if (!ui_edtActual.getText().toString().equals(user_password)){
            _activity.showAlertDialog(getString(R.string.confirm_pwd));
            return false;
        }

        if (ui_edtActual.getText().length() == 0) {
            _activity.showAlertDialog(getString(R.string.enter_actual));
            return false;

        } else if (ui_edtNueva.getText().length() == 0) {
            _activity.showAlertDialog(getString(R.string.enter_nueva));
            return false;

        } else if (ui_edtConta.getText().length() == 0){
            _activity.showAlertDialog(getString(R.string.enter_contrasena));
            return false ;

        } else if (ui_edtActual.getText().toString().length() < 6){
            _activity.showAlertDialog(getString(R.string.caracteres));
            return false;

        } else if (ui_edtConta.getText().toString().length() < 6){
            _activity.showAlertDialog(getString(R.string.caracteres));
            return false ;

        } else if (!ui_edtNueva.getText().toString().equals(ui_edtConta.getText().toString())){
            _activity.showAlertDialog(getString(R.string.nomatch));
            return false ;
        }
        return true;
    }

    public void ChangPassword(){

        String url = ReqConst.SERVER_URL + ReqConst.REQ_CHANGEPWD ;

        try {

            int userid = Commons.g_user.get_idx();

            String newPassword = ui_edtNueva.getText().toString().trim().replace(" ","%20");
            newPassword = URLEncoder.encode(newPassword,"utf-8");

            String params = String.format("/%d/%s", userid, newPassword);

            Log.d("ID*********************===>" , String.valueOf(userid));

            url += params ;

            Log.d("newPWD==============>", newPassword);

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        _activity.showProgress();

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String json) {

                parseChangePasswordResponse(json);
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {

                _activity.closeProgress();
                _activity.showAlertDialog(getString(R.string.error));
            }
        });

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT,0,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        CuponExpressApplication.getInstance().addToRequestQueue(stringRequest, url);
    }


    public void parseChangePasswordResponse(String json) {

        _activity.closeProgress();

        Log.d("json===>", json.toString());
        try {

            JSONObject response = new JSONObject(json);

            int result_code = response.getInt(ReqConst.RES_CODE);

            Log.d("ResponsRESULT===**************========>>>>", String.valueOf(result_code));
            Log.d("JsonRESUT***************************************>" ,json);

            if (result_code == ReqConst.CODE_SUCCESS){

                _activity.closeProgress();

                Preference.getInstance().put(_activity,
                        PrefConst.PREFKEY_USERPWD, ui_edtConta.getText().toString().trim());

                _activity.showToast("Successfully change password");

                _activity.configFragment();

            } else if (result_code == ReqConst.CODE_UNKNOWNERRO){

                _activity.closeProgress();
                _activity.showAlertDialog(getString(R.string.unknown));
            }

        } catch (JSONException e) {

            e.printStackTrace();

            _activity.showAlertDialog(getString(R.string.error));
        }
    }


    @Override
    public void onClick(View v) {

        switch (v.getId()){

//            case R.id.imv_back :
//                _activity.configFragment();
//
//                break;
            case R.id.txv_cambiar_cam:

                if (checkValue()){
                    ChangPassword();}

                break;

            case R.id.imv_back:
                _activity.configFragment();
        }

    }
}
