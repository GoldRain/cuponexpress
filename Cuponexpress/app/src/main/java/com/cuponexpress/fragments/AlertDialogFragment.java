package com.cuponexpress.fragments;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;

import com.cuponexpress.R;
import com.cuponexpress.main.MainActivity;

import java.util.Calendar;

public class AlertDialogFragment extends Fragment implements View.OnClickListener{

    MainActivity _acitivity ;

    View view ;

    EditText ui_edtCompla;
    TextView ui_txvTime, ui_txvCalendar,ui_txvCrear ;

    public AlertDialogFragment(MainActivity activity) {
        // Required empty public constructor
        this._acitivity = activity ;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_alert_dialog, container, false);

        laodLayout();

        return view ;
    }

    public void laodLayout() {

        LayoutInflater inflater = _acitivity.getLayoutInflater();
        final View alertLayout = inflater.inflate(R.layout.dialog_custom, null);

         ui_edtCompla = (EditText)alertLayout.findViewById(R.id.edt_cumple);

         ui_txvCalendar = (TextView)alertLayout.findViewById(R.id.txv_year);
        ui_txvCalendar.setOnClickListener(this);

         ui_txvTime = (TextView)alertLayout.findViewById(R.id.txv_time);
        ui_txvTime.setOnClickListener(this);

         ui_txvCrear = (TextView)alertLayout.findViewById(R.id.txv_crear);
        ui_txvCrear.setOnClickListener(this);


        final AlertDialog.Builder alert = new AlertDialog.Builder(_acitivity);

        alert.setView(alertLayout);
        alert.setCancelable(false);


        final AlertDialog dialog = alert.create() ;
        dialog.show();
        ui_txvCrear.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {

                _acitivity.registrarFragment();
                dialog.dismiss();

            }
        });
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.txv_year :

                showDatePickerDialog(v);
                break;
            case R.id.txv_time:
                showTimePickerDialog(v);
                break;
        }
    }

    public class DatePickerFragment extends DialogFragment
            implements DatePickerDialog.OnDateSetListener {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current date as the default date in the picker
            final Calendar c = Calendar.getInstance();
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);
            // Create a new instance of DatePickerDialog and return it
            return new DatePickerDialog(getActivity(), this, year, month, day);
        }

        public void onDateSet(DatePicker view, int yy, int mm, int dd) {populateSetDate(yy, mm+1, dd);}

        public void populateSetDate(int year, int month, int day) {
            String date = String.valueOf(day)+ "   -   " + String.valueOf(month) + "   -   " + String.valueOf(year);
            ui_txvCalendar.setText(date);
        }
    }


    public  class TimePickerFragment extends DialogFragment
            implements TimePickerDialog.OnTimeSetListener {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current time as the default values for the picker
            final Calendar c = Calendar.getInstance();
            int hour = c.get(Calendar.HOUR_OF_DAY);
            int minute = c.get(Calendar.MINUTE);

            // Create a new instance of TimePickerDialog and return it
            return new TimePickerDialog(getActivity(), this, hour, minute,
                    DateFormat.is24HourFormat(getActivity()));
        }

        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
            // Do something with the time chosen by the user
            populateSetTime(hourOfDay, minute);
        }

        public void populateSetTime(int hourofDay, int minute) {
            String date = String.valueOf(hourofDay)+ " : " + String.valueOf(minute) ;
            ui_txvTime.setText(date);

        }
    }

    public void showTimePickerDialog(View v) {
        DialogFragment newFragment = new TimePickerFragment();
        newFragment.show(_acitivity.getFragmentManager(), "timePicker");
    }

    public void showDatePickerDialog(View v) {
        DialogFragment newFragment = new DatePickerFragment();
        newFragment.show(_acitivity.getFragmentManager(), "datePicker");
    }

}