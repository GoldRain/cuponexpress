package com.cuponexpress.fragments;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.TimePickerDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.TimePicker;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.cuponexpress.CuponExpressApplication;
import com.cuponexpress.R;
import com.cuponexpress.base.BaseFragment;
import com.cuponexpress.commons.Commons;
import com.cuponexpress.commons.Constants;
import com.cuponexpress.commons.ReqConst;
import com.cuponexpress.main.MainActivity;
import com.cuponexpress.preference.PrefConst;
import com.cuponexpress.preference.Preference;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Calendar;

import static java.lang.String.valueOf;

public class RegistrarFragment extends BaseFragment implements View.OnClickListener{

    MainActivity _activity;
    View view ;

    String year1, time1;

    ImageView ui_imvBack ;
    TextView ui_txvText1, ui_txvText2, ui_txvText3, ui_txvText4, ui_txvText5 ;

    LinearLayout ui_lytFirst, ui_lytSecond, ui_lytThird, ui_lytForth, ui_lytFifth ;

    EditText ui_edtCompla;
    TextView ui_txvTime, ui_txvCalendar,ui_txvCrear ;

    int position = 0;
    int sequence = 0;

    public RegistrarFragment(MainActivity activity) {this._activity = activity; }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_registrar, container, false);
        // Inflate the layout for this fragment

        loadLayout();

        return view ;
    }

    boolean _isFromLogout = false ;

    private void loadLayout() {

        ui_imvBack = (ImageView)_activity.findViewById(R.id.imv_back);
        ui_imvBack.setOnClickListener(this);

        ui_lytFirst = (LinearLayout)view.findViewById(R.id.lyt_registrar1_reg) ;
        ui_lytFirst.setOnClickListener(this);
        ui_txvText1 = (TextView)view.findViewById(R.id.txv_registrar1_reg);

        ui_lytSecond = (LinearLayout)view.findViewById(R.id.lyt_registrar2_reg);
        ui_lytSecond.setOnClickListener(this);
        ui_txvText2 = (TextView)view.findViewById(R.id.txv_registrar2_reg);

        ui_lytThird = (LinearLayout)view.findViewById(R.id.lyt_registrar3_reg);
        ui_lytThird.setOnClickListener(this);
        ui_txvText3 = (TextView)view.findViewById(R.id.txv_registrar3_reg);

        ui_lytForth = (LinearLayout)view.findViewById(R.id.lyt_registrar4_reg);
        ui_lytForth.setOnClickListener(this);
        ui_txvText4 = (TextView)view.findViewById(R.id.txv_registrar4_reg);

        ui_lytFifth = (LinearLayout)view.findViewById(R.id.lyt_registrar5_reg) ;
        ui_lytFifth.setOnClickListener(this);
        ui_txvText5 = (TextView)view.findViewById(R.id.txv_registrar5_reg);


        if (Commons._title.length() > 0){

            ui_txvText1.setText(Commons._title);
            ui_txvText1.setTextColor(getResources().getColor(R.color.white));
        }


    }

                           /*AlertDialog*/
       /**/

             ////////////////////////////////////


    public void RecodeDate(){

        String url = ReqConst.SERVER_URL + ReqConst.REQ_RECORDDATE;

        try {

            int userid = Commons.g_user.get_idx();

            String title = ui_edtCompla.getText().toString().trim().replace(" ","%20");
            title = URLEncoder.encode(title,"utf-8");

            String date = String.valueOf(year1).toString() + "" +  String.valueOf(time1).toString();
            date.toString().trim().replace(" ","%20");
            date = URLEncoder.encode(date,"utf-8");
            int sequence = 0 ;

            String params = String.format("/%d/%s/%S/%d", userid, title, date, sequence);

            url += params ;

            Log.d("ID*********************===>" , String.valueOf(userid));
            Log.d("Time==================>>>", date);
            Log.d("Titl===================...>>>",title);
            Log.d("URL================********>>>>>",url);

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        _activity.showProgress();

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String json) {

                parseRecodeDate(json);
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {

                _activity.closeProgress();
                _activity.showAlertDialog(getString(R.string.error));
            }
        });

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT,0,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        CuponExpressApplication.getInstance().addToRequestQueue(stringRequest, url);
    }

    private void parseRecodeDate(String json) {

        Log.d("Result++==========>",json);

        _activity.closeProgress();

        try {
            JSONObject response = new JSONObject(json);

            int result_code = response.getInt(ReqConst.RES_CODE);
            Log.d("RESULT_code=========>>>", String.valueOf(result_code));

            if (result_code == ReqConst.CODE_SUCCESS){

                _activity.closeProgress();


                Preference.getInstance().put(_activity, PrefConst.PREFKEY_RECORDEDATE1_TITLE1,ui_edtCompla.getText().toString().trim());
                Preference.getInstance().put(_activity,PrefConst.PREFKEY_RECORDEDATE1_YEAR1, ui_txvCalendar.getText().toString().trim());
                Preference.getInstance().put(_activity,PrefConst.PREFKEY_RECORDEDATE1_TIME1, ui_txvTime.getText().toString().trim());

                _activity.showToast("Success");

            } else if (result_code == ReqConst.CODE_UNKNOWNERRO){
                _activity.closeProgress();
                _activity.showAlertDialog(getString(R.string.unknown));
            }

        } catch (JSONException e) {
            e.printStackTrace();
            _activity.showAlertDialog(getString(R.string.error));
        }
    }

    public void getUserRecordDate(){

        String url = ReqConst.SERVER_URL + ReqConst.REQ_GETUSERRECODE;

        int userid = Commons.g_user.get_idx();


        String params = String.format("/%d", userid);

        url += params ;

        Log.d("ID*********************===>" , String.valueOf(userid));
        Log.d("URL================********>>>>>",url);


        _activity.showProgress();

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String json) {

                parseGetUserRecodeDate(json);
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {

                _activity.closeProgress();
                _activity.showAlertDialog(getString(R.string.error));
            }
        });

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT,0,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        CuponExpressApplication.getInstance().addToRequestQueue(stringRequest, url);
    }

    public void parseGetUserRecodeDate(String json){

        _activity.closeProgress();

        try {
            JSONObject response = new JSONObject(json);

            int result_code = response.getInt(ReqConst.RES_CODE);
            Log.d("RESULT_code=========>>>", String.valueOf(result_code));

            if (result_code == ReqConst.CODE_SUCCESS){

                _activity.closeProgress();

               JSONObject recode_dates = response.getJSONObject(ReqConst.RES_RECORDDATE);

                int id = recode_dates.getInt(ReqConst.RES_IDX);
                int sequence = recode_dates.getInt(ReqConst.RES_SEQUENCE);
                String title = recode_dates.getString(ReqConst.RES_TITLE);
                String dateTime = recode_dates.getString(ReqConst.RES_DATATIME);

                _activity.showToast("Success");

            } else if (result_code == ReqConst.CODE_UNKNOWNERRO){
                _activity.closeProgress();
                _activity.showAlertDialog(getString(R.string.unknown));
            }

        } catch (JSONException e) {
            e.printStackTrace();
            _activity.showAlertDialog(getString(R.string.error));
        }
    }


    @Override
    public void onClick(View v) {

        switch (v.getId()){

            case R.id.imv_back:
                _activity.configFragment();
                break;

            case R.id.lyt_registrar1_reg:
                position = 1;
                AlertDialog();
                break;

            case R.id.lyt_registrar2_reg:
                position = 2;
                AlertDialog();
                break;

            case R.id.lyt_registrar3_reg:
                position = 3;
                AlertDialog();
                break;

            case R.id.lyt_registrar4_reg:
                position = 4;
                AlertDialog();
                break;

            case R.id.lyt_registrar5_reg:
                position = 5;
                AlertDialog();
                break;

            case R.id.txv_year :
                showDatePickerDialog(v);
                break;

            case R.id.txv_time:
                showTimePickerDialog(v);
                break;
        }
    }

    public void AlertDialog(){

        LayoutInflater inflater = _activity.getLayoutInflater();
        final View alertLayout = inflater.inflate(R.layout.dialog_custom, null);

        ui_edtCompla = (EditText)alertLayout.findViewById(R.id.edt_cumple);

        ui_txvCalendar = (TextView)alertLayout.findViewById(R.id.txv_year);
        ui_txvCalendar.setOnClickListener(this);

        ui_txvTime = (TextView)alertLayout.findViewById(R.id.txv_time);
        ui_txvTime.setOnClickListener(this);

        ui_txvCrear = (TextView)alertLayout.findViewById(R.id.txv_crear);
        ui_txvCrear.setOnClickListener(this);

        if (_isFromLogout){

            //   save user to empty
            Preference.getInstance().put(_activity, PrefConst.PREFKEY_RECORDEDATE1_TITLE1,"");
            Preference.getInstance().put(_activity,PrefConst.PREFKEY_RECORDEDATE1_YEAR1, "");
            Preference.getInstance().put(_activity,PrefConst.PREFKEY_RECORDEDATE1_TIME1, "");

            ui_edtCompla.setText("");
            ui_txvCalendar.setText("");
            ui_txvTime.setText("");

        } else {

             Commons._title = Preference.getInstance().getValue(_activity, PrefConst.PREFKEY_RECORDEDATE1_TITLE1, "");
            String _year = Preference.getInstance().getValue(_activity, PrefConst.PREFKEY_RECORDEDATE1_YEAR1, "");
            String _time = Preference.getInstance().getValue(_activity, PrefConst.PREFKEY_RECORDEDATE1_TIME1, "");

            ui_edtCompla.setText(Commons._title);
            ui_txvCalendar.setText(_year);
            ui_txvTime.setText(_time);

            Log.d("Titl, year, time =======>>>>>>>>>>>",Commons._title + _year + _time );
        }


        // container
        LinearLayout lytContainer = (LinearLayout) alertLayout.findViewById(R.id.lyt_container);
        lytContainer.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                // TODO Auto-generated method stub
                InputMethodManager imm = (InputMethodManager) _activity.getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(ui_edtCompla.getWindowToken(), 0);
                return false;
            }
        });



        final AlertDialog.Builder alert = new AlertDialog.Builder(_activity);

        alert.setView(alertLayout);
        alert.setCancelable(false);

        final AlertDialog dialog = alert.create() ;
        dialog.show();
        dialog.setCancelable(true);

        ui_txvCrear.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {

                if(!checkValue()){}

                else {

                if(position==1 ){
                    ui_txvText1.setText(ui_edtCompla.getText().toString().trim());
                    ui_txvText1.setTextColor(getResources().getColor(R.color.white));

                } else if (position == 2){
                    ui_txvText2.setText(ui_edtCompla.getText().toString().trim());
                    ui_txvText2.setTextColor(getResources().getColor(R.color.white));

                } else if (position == 3 ){
                    ui_txvText3.setText(ui_edtCompla.getText().toString().trim());
                    ui_txvText3.setTextColor(getResources().getColor(R.color.white));

                } else if (position == 4){
                    ui_txvText4.setText(ui_edtCompla.getText().toString().trim());
                    ui_txvText4.setTextColor(getResources().getColor(R.color.white));

                } else if (position == 5 ){
                    ui_txvText5.setText(ui_edtCompla.getText());
                    ui_txvText5.setTextColor(getResources().getColor(R.color.white));
                }
                    dialog.dismiss();
                    RecodeDate();
                }
            }

        });

    }

    public boolean checkValue(){

        if (ui_edtCompla.getText().toString().length() == 0) {
            _activity.showAlertDialog(getString(R.string.set_content));
            return false;

        } else if (ui_txvCalendar.getText().toString().length() == 0) {
            _activity.showAlertDialog(getString(R.string.set_year));
            return false;

        }  if (ui_txvTime.getText().toString().length() == 0) {
            _activity.showAlertDialog(getString(R.string.set_time));
            return false;

        }
        return true;

    }

    public class DatePickerFragment extends DialogFragment
            implements DatePickerDialog.OnDateSetListener {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current date as the default date in the picker
            final Calendar c = Calendar.getInstance();
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);
            // Create a new instance of DatePickerDialog and return it
            return new DatePickerDialog(getActivity(), this, year, month, day);
        }

        public void onDateSet(DatePicker view, int yy, int mm, int dd) {populateSetDate(yy, mm+1, dd);}

        public void populateSetDate(int year, int month, int day) {
            String date = String.valueOf(year)+ "   -   " + String.valueOf(month) + "   -   " + String.valueOf(day);
            ui_txvCalendar.setText(date);
            year1 = String.valueOf(year)+ "-" + String.valueOf(month) + "-" + String.valueOf(day);
        }
    }


    public  class TimePickerFragment extends DialogFragment
            implements TimePickerDialog.OnTimeSetListener {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current time as the default values for the picker
            final Calendar c = Calendar.getInstance();
            int hour = c.get(Calendar.HOUR_OF_DAY);
            int minute = c.get(Calendar.MINUTE);

            // Create a new instance of TimePickerDialog and return it
            return new TimePickerDialog(getActivity(), this, hour, minute,
                    DateFormat.is24HourFormat(getActivity()));
        }

        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
            // Do something with the time chosen by the user
            populateSetTime(hourOfDay, minute);
        }

        public void populateSetTime(int hourofDay, int minute) {
            String date = String.valueOf(hourofDay)+ " : " + String.valueOf(minute) ;
            ui_txvTime.setText(date);
            time1 =  String.valueOf(hourofDay)+ ":" + String.valueOf(minute) ;

        }
    }

    public void showTimePickerDialog(View v) {
        DialogFragment newFragment = new RegistrarFragment.TimePickerFragment();
        newFragment.show(_activity.getFragmentManager(), "timePicker");
    }

    public void showDatePickerDialog(View v) {
        DialogFragment newFragment = new RegistrarFragment.DatePickerFragment();
        newFragment.show(_activity.getFragmentManager(), "datePicker");
    }





}
