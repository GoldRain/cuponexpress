package com.cuponexpress.fragments;

import android.content.Context;
import android.media.Image;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.cuponexpress.R;
import com.cuponexpress.base.BaseFragment;
import com.cuponexpress.main.MainActivity;

public class AyudaFragment extends BaseFragment implements View.OnClickListener{

    MainActivity _activity ;
    View view ;
    ImageView ui_imvBack ;

    public AyudaFragment(MainActivity activity) {this._activity = activity ;}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        /*return inflater.inflate(R.layout.fragment_ayuda, container, false);*/

        view = inflater.inflate(R.layout.fragment_ayuda, container, false);

        loadLayout();

        return view ;
    }

    private void loadLayout() {

        ui_imvBack = (ImageView)_activity.findViewById(R.id.imv_back);
        ui_imvBack.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){

            case R.id.imv_back:
                _activity.configFragment();
        }
    }
}
