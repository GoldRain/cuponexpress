package com.cuponexpress.fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.ImageView;


import com.cuponexpress.R;
import com.cuponexpress.adapter.GridView_CommonsAdapter;
import com.cuponexpress.base.BaseFragment;
import com.cuponexpress.main.MainActivity;

import java.util.ArrayList;

public class DeporteFragment extends BaseFragment {

    MainActivity _activity ;
    View view ;

    GridView_CommonsAdapter deporte_adapter;
    ArrayList<Integer> img_depor = new ArrayList<>();
    GridView ui_gridView_depor;

    ImageView ui_imvBack ;

    public DeporteFragment(MainActivity activity) {
        // Required empty public constructor

        this._activity = activity;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_deporte, container, false);

        loadLayout();
        loadDate();

        return view ;
    }
    private void loadDate() {

        img_depor.add(R.drawable.img_c1); img_depor.add(R.drawable.img_c2); img_depor.add(R.drawable.img_c3); img_depor.add(R.drawable.img_c4);
        img_depor.add(R.drawable.img_c5); img_depor.add(R.drawable.img_c6); img_depor.add(R.drawable.img_c7); img_depor.add(R.drawable.img_c8);
        img_depor.add(R.drawable.img_c9); img_depor.add(R.drawable.img_c2); img_depor.add(R.drawable.img_c3); img_depor.add(R.drawable.img_c4);
        img_depor.add(R.drawable.img_c5); img_depor.add(R.drawable.img_c6); img_depor.add(R.drawable.img_c7); img_depor.add(R.drawable.img_c8);
    }

    private void loadLayout() {

        ui_imvBack = (ImageView)_activity.findViewById(R.id.imv_back_cat);
        ui_imvBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                _activity.categoryFragment();
            }
        });

        deporte_adapter = new GridView_CommonsAdapter(_activity,img_depor);
        ui_gridView_depor = (GridView)view.findViewById(R.id.grd_deporte);
        ui_gridView_depor.setAdapter(deporte_adapter);
    }


}
