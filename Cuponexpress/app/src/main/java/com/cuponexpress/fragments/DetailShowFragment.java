package com.cuponexpress.fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.cuponexpress.R;
import com.cuponexpress.main.MainActivity;


public class DetailShowFragment extends Fragment {

    MainActivity _activity ;
    View view ;

    ImageView ui_imvBack ;

    public DetailShowFragment( MainActivity activity) {

        this._activity = activity ;
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_detail_show, container, false);

        ui_imvBack = (ImageView)view.findViewById(R.id.imv_back_details);
        ui_imvBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                _activity.showFragment();
                _activity.overridePendingTransition(R.anim.left_in, R.anim.right_out);
                _activity.overridePendingTransition(0,0);
            }
        });

        return view ;
    }

}

