package com.cuponexpress.main;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.cuponexpress.R;
import com.cuponexpress.base.CommonActivity;
import com.cuponexpress.commons.Commons;
import com.cuponexpress.commons.Constants;
import com.cuponexpress.model.UserEntity;
import com.cuponexpress.preference.PrefConst;
import com.cuponexpress.preference.Preference;

public class IntroOnActivity extends CommonActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intro_on);

        isWifiEnabled();

        if (isWifiEnabled()){

            String name = Preference.getInstance().getValue(this,
                    PrefConst.PREFKEY_USERNAME, "");

            if(name.length()>0){

                Commons.g_user = new UserEntity();

                String email = Preference.getInstance().getValue(this,
                        PrefConst.PREFKEY_USEREMAIL, "");

                String photourl = Preference.getInstance().getValue(this,
                        PrefConst.PREFKEY_PHOTURL, "");

                String id = Preference.getInstance().getValue(this,
                        PrefConst.PREFKEY_ID, "");

                Log.d("introid===",id);

                int idx = 0;

                try {
                    idx = Integer.parseInt(id);
                } catch(NumberFormatException nfe){}

                Commons.g_user.set_idx(idx);
                Commons.g_user.set_username(name);
                Commons.g_user.set_email(email);
                Commons.g_user.set_photoUrl(photourl);

                Log.d("photo===========splash",photourl);

                gotomain();

            }else {

                gotoLogin();
            }

        } else {

           new Handler().postDelayed(new Runnable() {
               @Override
               public void run() {
                   startActivity(new Intent(IntroOnActivity.this, IntroActivity.class));
                   finish();
               }

           },1000);
        }
    }

    public boolean isWifiEnabled(){

        ConnectivityManager cm = (ConnectivityManager) getSystemService(
                Context.CONNECTIVITY_SERVICE);
        NetworkInfo wifiNetwork = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);

        if (wifiNetwork != null && wifiNetwork.isConnected()) {

            return true;
        }

        return false;
    }

    private void gotoLogin() {

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                Intent intent = new Intent(IntroOnActivity.this , SignInActivity.class);
                startActivity(intent);

                finish();
            }
        }, 1000);
    }
    public void gotomain(){

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                Intent intent = new Intent(IntroOnActivity.this , MainActivity.class);
                startActivity(intent);

                finish();
            }
        }, 1000);
    }
}
