package com.cuponexpress.main;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.util.Patterns;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.cuponexpress.CuponExpressApplication;
import com.cuponexpress.R;
import com.cuponexpress.base.CommonActivity;
import com.cuponexpress.commons.Constants;
import com.cuponexpress.commons.ReqConst;

import org.json.JSONException;
import org.json.JSONObject;

public class ForgotActivity extends CommonActivity implements View.OnClickListener {

    ImageView ui_imvBack ;
    EditText ui_edtEmail;
    TextView ui_txvResend;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot);

        loadLayout();
    }

    private void loadLayout() {

        ui_imvBack = (ImageView)findViewById(R.id.imv_back);
        ui_imvBack.setOnClickListener(this);

        ui_edtEmail = (EditText)findViewById(R.id.edt_email);

        ui_txvResend = (TextView)findViewById(R.id.txv_resend);
        ui_txvResend.setOnClickListener(this);

        LinearLayout linearLayout = (LinearLayout)findViewById(R.id.lyt_container);
        linearLayout.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {

                //TODO Auto-generated method stub
                InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(ui_edtEmail.getWindowToken(),0);
                return false;
            }
        });
    }

    public boolean checkValid(){

        if (ui_edtEmail.getText().toString().length() == 0){
            ui_edtEmail.setError(getString(R.string.enter_email));
            return false ;

        } else if (ui_edtEmail.getText().toString().length() == 0 ||
                !Patterns.EMAIL_ADDRESS.matcher(ui_edtEmail.getText().toString()).matches()){
            ui_edtEmail.setError(getString(R.string.wrong_email));
            return false ;
        }
        return true;
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()){

            case R.id.imv_back:
                finish();
                break;

            case R.id.txv_resend:
                if (checkValid()){
                    getPassword();
                showToast("Resend");}
                break;
        }

    }

    public void getPassword() {

        String url = ReqConst.SERVER_URL + ReqConst.REQ_RECOVERYPWD;
        String emai = ui_edtEmail.getText().toString().trim();
        String params = String.format("/%s",emai);

        url += params ;

        Log.d("forgoturl===",url);
        Log.d("Email++++++++++++++++>>>",emai);

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String json) {
                parseGetPassword(json);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                closeProgress();
                showAlertDialog(getString(R.string.error));
            }
        });

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT,0,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        CuponExpressApplication.getInstance().addToRequestQueue(stringRequest,url);
    }

    public void parseGetPassword(String json) {

        closeProgress();
        Log.d("forgotresponse====",json);

        try {

            JSONObject response = new JSONObject(json);

            int result_code = response.getInt(ReqConst.RES_CODE);

            Log.d("ForgotResutl+++++++++", String.valueOf(result_code));

            if (result_code == ReqConst.CODE_SUCCESS){
                gotoVerificationActivity();

            } else if (result_code == ReqConst.CODE_UNREGUSER){
                showAlertDialog(getString(R.string.unregistered_user));
            }

        } catch (JSONException e) {
            e.printStackTrace();
            showAlertDialog(getString(R.string.error));
        }
    }

    public void gotoVerificationActivity() {

        Intent intent = new Intent(this, SignInActivity.class);
        intent.putExtra(Constants.EMAIL, ui_edtEmail.getText().toString().trim());
        startActivity(intent);
        overridePendingTransition(0,0);
        finish();
    }
}
