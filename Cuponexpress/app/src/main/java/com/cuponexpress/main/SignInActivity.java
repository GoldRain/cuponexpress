package com.cuponexpress.main;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Bundle;

import android.util.Base64;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.cuponexpress.CuponExpressApplication;
import com.cuponexpress.R;
import com.cuponexpress.base.CommonActivity;
import com.cuponexpress.commons.Commons;
import com.cuponexpress.commons.Constants;
import com.cuponexpress.commons.ReqConst;
import com.cuponexpress.model.UserEntity;
import com.cuponexpress.preference.PrefConst;
import com.cuponexpress.preference.Preference;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.jar.Attributes;

import static java.lang.String.valueOf;


public class SignInActivity extends CommonActivity implements View.OnClickListener {

    TextView ui_txvFacebook, ui_txvSignin, ui_txvSignup ,ui_txvForgot ;
    EditText ui_edtUsername, ui_edtPwd ;

    public static final int REGISTER_CODE = 100;

    private String FEmail, Name,Firstname, Lastname,Id,Gender,Image_url;
    String photourl;

    private UserEntity _user;
    String  _name = "", _password = "";

    public static CallbackManager callbackManager;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        FacebookSdk.sdkInitialize(getApplicationContext());
        callbackManager = CallbackManager.Factory.create();

        setContentView(R.layout.activity_signin);


        try
        {

            PackageInfo info = getPackageManager().getPackageInfo("com.cuponexpress", PackageManager.GET_SIGNATURES);

            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");

                md.update(signature.toByteArray());
                Log.i("KeyHash::", Base64.encodeToString(md.digest(), Base64.DEFAULT));//will give developer key hash
//                Toast.makeText(getApplicationContext(), Base64.encodeToString(md.digest(), Base64.DEFAULT), Toast.LENGTH_LONG).show(); //will give app key hash or release key hash

            }
        } catch (PackageManager.NameNotFoundException e) {

        } catch (NoSuchAlgorithmException e) {

        }

        initValue();
        loadLayout();
    }

    boolean _isFromLogout = false ;

    private void initValue() {

        Intent intent = getIntent();

        try {
            _isFromLogout = intent.getBooleanExtra(Constants.KEY_LOGOUT, false);

        } catch (Exception e){
        }
    }

    private void loadLayout() {

        ui_txvFacebook = (TextView)findViewById(R.id.txv_facebook);
        ui_txvFacebook.setOnClickListener(this);

        ui_txvSignin = (TextView)findViewById(R.id.txv_signin);
        ui_txvSignin.setOnClickListener(this);

        ui_txvSignup = (TextView)findViewById(R.id.txv_signup);
        ui_txvSignup.setOnClickListener(this);

        ui_edtUsername = (EditText)findViewById(R.id.edt_name);
        /*ui_edtUsername.setHint(getString(R.string.username));*/

        ui_edtPwd = (EditText)findViewById(R.id.edt_pwd);

        ui_txvForgot = (TextView)findViewById(R.id.txv_forgot);
        ui_txvForgot.setOnClickListener(this);

        // container
        LinearLayout lytContainer = (LinearLayout) findViewById(R.id.lyt_container);
        lytContainer.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                // TODO Auto-generated method stub
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(ui_edtPwd.getWindowToken(), 0);
                return false;
            }
        });

        if (_isFromLogout){

            //   save user to empty
            Preference.getInstance().put(this, PrefConst.PREFKEY_USERNAME,"");
            Preference.getInstance().put(this,PrefConst.PREFKEY_USERPWD, "");

            ui_edtUsername.setText("");
            ui_edtPwd.setText("");

        } else {

            String _name = Preference.getInstance().getValue(this, PrefConst.PREFKEY_USERNAME, "");
            String _password = Preference.getInstance().getValue(this, PrefConst.PREFKEY_USERPWD, "");

            ui_edtUsername.setText(_name);
            ui_edtPwd.setText(_password);

            Log.d("UserName==========>>>>>>>>>>>",valueOf( ui_edtUsername.getText().toString()));
            Log.d("Password==========>>>>>>>>>>>",valueOf(ui_edtPwd.getText().toString()));


            if ( _name.length()>0 && _password.length() > 0 ) {

                processLogin();
            }
        }

    }

    /*check the empty string*/
    public boolean checkValue(){

        if (ui_edtUsername.getText().length() == 0) {
            showAlertDialog(getString(R.string.enter_name));
            return false;
        } else if (ui_edtPwd.getText().length() == 0) {
            showAlertDialog(getString(R.string.enter_password));
            return false;
        }

        return true;
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()){

            case R.id.txv_facebook:

                loginWithFB();
                break;

            case R.id.txv_signin:

                if (checkValue()) {
                    processLogin();
                }
                break;

            case R.id.txv_signup:

                gotoSignupActivity();
                break;

            case R.id.txv_forgot:
                gotoForgotActivity();
                break;
        }

    }

    public void processLogin(){

        String url = ReqConst.SERVER_URL + ReqConst.REQ_LOGIN;

        try {
            String username = ui_edtUsername.getText().toString().trim().replace(" ","%20");
            username = URLEncoder.encode(username,"utf-8");

            String password = ui_edtPwd.getText().toString().trim().replace(" ","%20");
            password = URLEncoder.encode(password,"utf-8");

            int device_type = 0;

            String params = String.format("/%s/%s/%d", username, password, device_type);
            url += params ;

            Log.d("loginURL===========********======>", url);

        } catch (UnsupportedEncodingException e) {

            closeProgress();
            e.printStackTrace();}

        showProgress();

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {

            @Override
            public void onResponse(String json) {

                parseSigninResponse(json);
            }

        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {

                closeProgress();
                showAlertDialog(getString(R.string.error));

            }
        });

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        CuponExpressApplication.getInstance().addToRequestQueue(stringRequest, url);

    }

    public void parseSigninResponse(String json) {

        closeProgress();

        try {

            JSONObject response = new JSONObject(json);

            int result_code = response.getInt(ReqConst.RES_CODE);

            Log.d("ResponseRESULT=====**********=========>", json);
            Log.d("RESPONS++++RESULT==========>..>>>>", String.valueOf(result_code));

            if (result_code == ReqConst.CODE_SUCCESS){

                closeProgress();

                JSONObject user = response.getJSONObject(ReqConst.RES_USERINFO);

                _user = new UserEntity();

                _user.set_idx(user.getInt(ReqConst.RES_ID));
                Log.d("id======", String.valueOf(_user.get_idx()));
                _user.set_username(user.getString(ReqConst.RES_USERNAME));
                _user.set_email(user.getString(ReqConst.RES_EMAIL));
                _user.set_password(ui_edtPwd.getText().toString());
                _user.set_photoUrl(user.getString(ReqConst.RES_PHOTO_URL));


                Preference.getInstance().put(this,
                        PrefConst.PREFKEY_USERNAME, ui_edtUsername.getText().toString().trim());

                Preference.getInstance().put(this,
                        PrefConst.PREFKEY_USERPWD, ui_edtPwd.getText().toString().trim());

                Commons.g_user = _user ;

                gotoWelcomeActivity();

            } else if(result_code == ReqConst.CODE_UNKNOWNERRO){

                closeProgress();

                showAlertDialog(getString(R.string.unregistered_user));
                Log.d("Failded ======>", valueOf(result_code));

            }else if (result_code == ReqConst.CODE_WRONGPWD){

                closeProgress();
                showAlertDialog(getString(R.string.checkPwd));
                Log.d("Failded ======>", valueOf(result_code));

            } else if (result_code == ReqConst.CODE_UNREGUSER){
                closeProgress();
                showAlertDialog(getString(R.string.unregiuser));
            }

        } catch (JSONException e) {
            closeProgress();
            e.printStackTrace();

            showAlertDialog(getString(R.string.error));
        }
    }

    //====================================Facebook Login Start======================================
    private void loginWithFB() {

        callbackManager = CallbackManager.Factory.create();

        // set permissions
        LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("email", "user_photos", "public_profile"));

        LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                AccessToken accessToken = loginResult.getAccessToken();
                Profile profile = Profile.getCurrentProfile();

                // Facebook Email address
                GraphRequest request = GraphRequest.newMeRequest(
                        loginResult.getAccessToken(),
                        new GraphRequest.GraphJSONObjectCallback() {
                            @Override
                            public void onCompleted(
                                    JSONObject object,
                                    GraphResponse response) {
                                Log.v("LoginActivity Response ", response.toString());

                                try {
                                    Name = object.getString("name");
                                    Name.replace(" ", "");
                                    Id=object.getString("id");
                                    Firstname=object.getString("first_name");
                                    Lastname=object.getString("last_name");
                                    Gender=object.getString("gender");
                                    FEmail = object.getString("email");
                                    Image_url="http://graph.facebook.com/(Id)/picture?type=large";
                                    Image_url=URLEncoder.encode(Image_url);
                                    Log.d("Email = ", " " + FEmail);
                                    Log.d("Name======",Name);
                                    Log.d("firstName======",Firstname);
                                    Log.d("lastName======",Lastname);
                                    Log.d("Gender======",Gender);
                                    Log.d("id======",Id);

                                    photourl = "http://graph.facebook.com/"+Id+"/picture?type=large";
                                    SocialLogin(Name,FEmail,photourl,0);

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        });
                Bundle parameters = new Bundle();
                parameters.putString("fields", "id,name,first_name,last_name,email,gender, birthday");
                request.setParameters(parameters);
                request.executeAsync();

            }

            @Override
            public void onCancel() {
                LoginManager.getInstance().logOut();
            }

            @Override
            public void onError(FacebookException error) {

            }

        });


    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        callbackManager.onActivityResult(requestCode, resultCode, data);

    }

    //==================================Face book Login End====================================



    //====================================== Social Login Start =================================

    public void SocialLogin(String username, String useremail, final String photo_url, int device_type){


        showProgress();

        String url = ReqConst.SERVER_URL + ReqConst.REQ_FACEBOOK ;
        StringRequest myRequest = new StringRequest(
                Request.Method.POST, url ,new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                Log.d("response=====", String.valueOf(response));
                parseEditResponse(response);
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("error============", String.valueOf(error));
                        closeProgress();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                try {
                    params.put("id",String.valueOf(Commons.g_user.get_idx()));
                    params.put("username",Name);
                    params.put("useremail",  FEmail);
                    params.put("photo_url", photo_url);
                    params.put("device_type", String.valueOf(0));


                } catch (Exception e) {}
                return params;
            }
        };

        myRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        CuponExpressApplication.getInstance().addToRequestQueue(myRequest,url);  // url or "tag"


    }


    private void parseEditResponse(String response) {

        Log.d("respons", response);

        try {

            JSONObject object = new JSONObject(response);

            int result_code = object.getInt(ReqConst.RES_CODE);


            if (result_code == ReqConst.CODE_SUCCESS) {
                Commons.g_user=new UserEntity();

                /*showToast("Success change your profile !");*/

                int id=object.getInt("id");
                Log.d("id====", String.valueOf(id));
                Commons.g_user.set_idx(object.getInt("id"));

                Commons.g_user.set_username(Name);
                Commons.g_user.set_email(FEmail);
               Commons.g_user.set_photoUrl(photourl);
                Preference.getInstance().put(this, PrefConst.PREFKEY_USERNAME,Name);
                Preference.getInstance().put(this, PrefConst.PREFKEY_USEREMAIL,FEmail);
                Preference.getInstance().put(this, PrefConst.PREFKEY_PHOTURL,photourl);
                Preference.getInstance().put(this, PrefConst.PREFKEY_ID,String.valueOf(id));

                gotomain();

                closeProgress();

            }

        } catch (JSONException e) {

            e.printStackTrace();

           showAlertDialog("Network Error. Please Try Again.");
           closeProgress();
        }

    }

    public void gotomain(){

                Intent intent = new Intent(SignInActivity.this , MainActivity.class);
                startActivity(intent);

                finish();

    }

    //================================Social Login End======================================


    private void gotoWelcomeActivity() {

        Intent intent = new Intent(this ,WelcomeActivity.class);
        startActivity(intent);
        finish();
    }

    private void gotoSignupActivity() {

        Intent intent = new Intent(this , SignUpActivity.class);
        /*overridePendingTransition(0,0);*/
        startActivityForResult(intent,REGISTER_CODE);
        finish();
    }

    private void gotoForgotActivity() {

        Intent intent = new Intent(this, ForgotActivity.class);
        overridePendingTransition(0,0);
        startActivity(intent);
    }


   /* @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {

            case REGISTER_CODE:

                if (resultCode == RESULT_OK) {

                    // load saved user
                    String name = Preference.getInstance().getValue(this,
                            PrefConst.PREFKEY_USERNAME, "");
                    String userpwd = Preference.getInstance().getValue(this,
                            PrefConst.PREFKEY_USERPWD, "");

                    ui_edtUsername.setText(name);
                    ui_edtPwd.setText(userpwd);

                    *//*gotoWelcomeActivity();*//*
                    processWelcome();
                }
                break;
        }
    }*/

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK){

            onExit();
            return true ;
        }

        return super.onKeyDown(keyCode,event);
    }

/*    @Override
    protected void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }*/

    @Override
    public void onBackPressed() {
        onExit();
    }
}
