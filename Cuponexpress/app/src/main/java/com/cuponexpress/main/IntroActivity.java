package com.cuponexpress.main;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Handler;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.cuponexpress.commons.Commons;
import com.cuponexpress.main.ProgressbarTask;
import com.cuponexpress.R;
import com.cuponexpress.base.CommonActivity;
import com.cuponexpress.commons.Constants;
import com.cuponexpress.model.UserEntity;
import com.cuponexpress.preference.PrefConst;
import com.cuponexpress.preference.Preference;

import java.util.Timer;
import java.util.TimerTask;

import static android.R.attr.value;

public class IntroActivity extends CommonActivity {

    final Handler mHandler = new Handler();
    Timer mTimer = new Timer();
    int percent =0;
    TextView txvpercent;
    ProgressBar progressBar ;
    Thread mThread ;
    View v ;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intro);

        progressBar = (ProgressBar)findViewById(R.id.progressbar);
        mTimer.schedule(doAsynchronousTask, 0, 200);
        txvpercent=(TextView)findViewById(R.id.txv_percent);

        startProgress(v);

        String name = Preference.getInstance().getValue(this,
                PrefConst.PREFKEY_USERNAME, "");

        if(name.length()>0){

            Commons.g_user = new UserEntity();

            String email = Preference.getInstance().getValue(this,
                    PrefConst.PREFKEY_USEREMAIL, "");

            String photourl = Preference.getInstance().getValue(this,
                    PrefConst.PREFKEY_PHOTURL, "");

           String id = Preference.getInstance().getValue(this,
                    PrefConst.PREFKEY_ID, "");
            Log.d("introid===",id);

            int idx = 0;

            try {
                idx = Integer.parseInt(id);
            } catch(NumberFormatException nfe){}

            Commons.g_user.set_idx(idx);
            Commons.g_user.set_username(name);
            Commons.g_user.set_email(email);
            Commons.g_user.set_photoUrl(photourl);

            Log.d("photo===========splash",photourl);

            gotomain();

        }else {

            gotoLogin();
        }

    }


    public void startProgress(View v){

        if (mThread == null){

        mThread = new Thread(new ProgressbarTask(v, mOnProgressBarListener));
        mThread.start();
        }
    }

    ProgressbarTask.OnProgressBarListener mOnProgressBarListener = new ProgressbarTask.OnProgressBarListener() {
        @Override
        public void setProgress(int value, View v) {

            progressBar.setProgress(value);
        }

        @Override
        public void onProgressFinish(View v) {

            mThread = null ;
        }
    };

    TimerTask doAsynchronousTask = new TimerTask() {

        @Override
        public void run() {
            mHandler.post(new Runnable() {
                @Override
                public void run() {
                    percent+=4;
                    if(percent<200) {
                        txvpercent.setText(percent + "%");
                    }else {
                        txvpercent.setText("99%");
                    }

                }
            });
        }
    };

    private void gotoLogin() {

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                Intent intent = new Intent(IntroActivity.this , SignInActivity.class);
                startActivity(intent);

                finish();
            }
        }, Constants.SPLASH_TIME);
    }
    public void gotomain(){
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                Intent intent = new Intent(IntroActivity.this , MainActivity.class);
                startActivity(intent);

                finish();
            }
        }, Constants.SPLASH_TIME);
    }

}
