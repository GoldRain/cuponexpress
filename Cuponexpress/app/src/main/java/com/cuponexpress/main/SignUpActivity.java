package com.cuponexpress.main;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.MediaStore;
import android.os.Bundle;
import android.util.Log;
import android.util.Patterns;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.cuponexpress.CuponExpressApplication;
import com.cuponexpress.R;
import com.cuponexpress.base.CommonActivity;
import com.cuponexpress.commons.Commons;
import com.cuponexpress.commons.Constants;
import com.cuponexpress.commons.ReqConst;
import com.cuponexpress.model.UserEntity;
import com.cuponexpress.preference.PrefConst;
import com.cuponexpress.preference.Preference;
import com.cuponexpress.utils.BitmapUtils;
import com.cuponexpress.utils.MultiPartRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

import static java.lang.String.valueOf;

public class SignUpActivity extends CommonActivity implements View.OnClickListener {

    EditText ui_edtUsername, ui_edtEmail, ui_edtPwd, ui_edtConpwd;
    ImageView ui_imvBack;
    TextView ui_txvSignup;

    CircleImageView ui_imvPhoto;
    Uri _imageCaptureUri;
    String _photoPath = "";
    private int _idx = 0;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        Commons.g_newUser = null ;

        loadLayout();
    }

    private void loadLayout() {

        ui_edtUsername = (EditText)findViewById(R.id.edt_name);
        ui_edtEmail = (EditText)findViewById(R.id.edt_email);

        ui_edtPwd = (EditText)findViewById(R.id.edt_pwd);
        ui_edtConpwd = (EditText)findViewById(R.id.edt_confirmpwd);

        ui_txvSignup = (TextView)findViewById(R.id.txv_signup);
        ui_txvSignup.setOnClickListener(this);

        ui_imvBack = (ImageView)findViewById(R.id.imv_back);
        ui_imvBack.setOnClickListener(this);

        ui_imvPhoto = (CircleImageView) findViewById(R.id.imv_photo);
        ui_imvPhoto.setOnClickListener(this);

        LinearLayout linearLayout = (LinearLayout)findViewById(R.id.lyt_container);
        linearLayout.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {

                //TODO Auto-generated method stub
                InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(ui_edtUsername.getWindowToken(),0);
                return false;
            }
        });
    }

    private void selectPhoto() {

        final String[] items = {"Take photo", "Choose from Gallery","Cancel"};

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setItems(items, new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int item) {

                if (item == 0) {
                    doTakePhoto();
                } else if(item == 1){
                    doTakeGallery();

                }else {
                    return;
                }
            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }

    public void doTakePhoto() {

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        String picturePath = BitmapUtils.getTempFolderPath() + "photo_temp.jpg";
        _imageCaptureUri = Uri.fromFile(new File(picturePath));

        intent.putExtra(MediaStore.EXTRA_OUTPUT, _imageCaptureUri);
        startActivityForResult(intent, Constants.PICK_FROM_CAMERA);

    }

    private void doTakeGallery() {

        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.setType(MediaStore.Images.Media.CONTENT_TYPE);
        startActivityForResult(intent, Constants.PICK_FROM_ALBUM);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data){

        switch (requestCode){

            case Constants.CROP_FROM_CAMERA: {

                if (resultCode == RESULT_OK){
                    try {

                        File saveFile = BitmapUtils.getOutputMediaFile(this);

                        InputStream in = getContentResolver().openInputStream(Uri.fromFile(saveFile));
                        BitmapFactory.Options bitOpt = new BitmapFactory.Options();
                        Bitmap bitmap = BitmapFactory.decodeStream(in, null, bitOpt);
                        in.close();

                        //set The bitmap data to image View
                        ui_imvPhoto.setImageBitmap(bitmap);
                        _photoPath = saveFile.getAbsolutePath();

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                break;
            }
            case Constants.PICK_FROM_ALBUM:

                if (resultCode == RESULT_OK){
                    _imageCaptureUri = data.getData();
                }

            case Constants.PICK_FROM_CAMERA:
            {
                try {

                    _photoPath = BitmapUtils.getRealPathFromURI(this, _imageCaptureUri);

                    Intent intent = new Intent("com.android.camera.action.CROP");
                    intent.setDataAndType(_imageCaptureUri, "image/*");

                    intent.putExtra("crop", true);
                    intent.putExtra("scale", true);
                    intent.putExtra("outputX", Constants.PROFILE_IMAGE_SIZE);
                    intent.putExtra("outputY", Constants.PROFILE_IMAGE_SIZE);
                    intent.putExtra("aspectX", 1);
                    intent.putExtra("aspectY", 1);
                    intent.putExtra("noFaceDetection", true);
                    //intent.putExtra("return-data", true);
                    intent.putExtra("output", Uri.fromFile(BitmapUtils.getOutputMediaFile(this)));

                    startActivityForResult(intent, Constants.CROP_FROM_CAMERA);
                }catch (Exception e){
                    e.printStackTrace();
                }
                break;
            }
        }
    }

    public boolean checkValid(){

        if (_photoPath.length() == 0){
            showAlertDialog(getString(R.string.select_photo));
            return false ;}

        if (ui_edtUsername.getText().toString().length() == 0) {
            showAlertDialog(getString(R.string.enter_name));
            return false;

        } else if (ui_edtEmail.length() == 0){
            ui_edtEmail.setError(getString(R.string.enter_email));
            return false ;

        } else if (!Patterns.EMAIL_ADDRESS.matcher(ui_edtEmail.getText().toString()).matches()){
            ui_edtEmail.setError(getString(R.string.wrong_email));
            return false ;

        } else if (ui_edtPwd.getText().length() == 0) {
            showAlertDialog(getString(R.string.enter_password));
            return false;

        } else if(ui_edtConpwd.getText().length() == 0){
            showAlertDialog(getString(R.string.input_confirm));
            return false;

        }  else if (ui_edtConpwd.getText().toString().length() < 6 ){
            showAlertDialog(getString(R.string.PwdLength));
            return false;

        } else if(!ui_edtPwd.getText().toString().equals(ui_edtConpwd.getText().toString())){
            showAlertDialog(getString(R.string.checkPwd));
            return false;
        }

        return true ;
    }

    /*Signup the user information to server*/

    public void progressSignup() {

  //   Upload data============================

        String url = ReqConst.SERVER_URL + ReqConst.REQ_SIGNUP;

        try {

            String username = ui_edtUsername.getText().toString().trim().replace(" ","%20");
            username = URLEncoder.encode(username,"utf-8");

            String email = ui_edtEmail.getText().toString().trim().replace(" ","%20");
            email = URLEncoder.encode(email,"utf-8");

            int device_type = 0 ;

            String password = ui_edtPwd.getText().toString().trim().replace(" ","%20");
            password = URLEncoder.encode(password,"utf-8");
            Log.d("password++++++",password);

            String params = String.format("/%s/%s/%s/%d", username, email, password, device_type);
            url += params ;

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        showProgress();

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {

            @Override
            public void onResponse(String json) {

                parseSignupResponse(json);

            }

        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                closeProgress();
                showAlertDialog(getString(R.string.error));
            }
        });

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        CuponExpressApplication.getInstance().addToRequestQueue(stringRequest,url);

    }

    private void parseSignupResponse(String json) {   // download ===============

        closeProgress();

        try {

            JSONObject respons = new JSONObject(json);

            Log.d("signupresult======", String.valueOf(respons));

            int result_code = respons.getInt(ReqConst.RES_CODE);

            if (result_code == ReqConst.CODE_SUCCESS){

                _idx = respons.getInt(ReqConst.RES_ID);

                uploadImage();

            } else if (result_code == ReqConst.CODE_EXISTEMAIL){
                closeProgress();
                showAlertDialog(getString(R.string.enter_email));

            } else if (result_code == ReqConst.CODE_EXITNAME){
                closeProgress();
                showAlertDialog(getString(R.string.enter_name));

            } else if (result_code == ReqConst.CODE_UNKNOWNERRO){
                closeProgress();
                showAlertDialog(getString(R.string.error));
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void uploadImage(){

        //if no profile photo
        if (_photoPath.length() == 0){

            onSuccessSignup();
            return;
        }

        try {
            File file = new File(_photoPath);

            Map<String, String> params = new HashMap<>();
            params.put(ReqConst.PARAM_ID, String.valueOf(_idx));

            String url = ReqConst.SERVER_URL + ReqConst.REQ_UPLOADIMAGE ;

            MultiPartRequest reqMultiPart = new MultiPartRequest(url, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {

                    closeProgress();
                    showAlertDialog(getString(R.string.photo_upload_fail));
                }

            }, new Response.Listener<String>() {
                @Override
                public void onResponse(String json) {

                    Log.d("success============********>>", _photoPath);

                    ParseUploadImageResponse(json);

                }

            },file, ReqConst.PARAM_FILE, params);

            reqMultiPart.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT ,0 ,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            CuponExpressApplication.getInstance().addToRequestQueue(reqMultiPart,url);

        } catch (Exception e){

            e.printStackTrace();
            closeProgress();
            showAlertDialog(getString(R.string.photo_upload_fail));
        }
    }

    private void ParseUploadImageResponse(String json) {

        Log.d("responseimage====",String.valueOf(json));

        closeProgress();

        try {

            JSONObject response = new JSONObject(json);
            int result_code = response.getInt(ReqConst.RES_CODE);

            if (result_code == ReqConst.CODE_SUCCESS){
                onSuccessSignup();

            }else if (result_code == ReqConst.CODE_UPLOADFAIL){
                showAlertDialog(getString(R.string.photo_upload_fail));
            }
        } catch (JSONException e) {
            e.printStackTrace();
            showAlertDialog(getString(R.string.photo_upload_fail));
        }

        //onSuccessSignup();
    }

    public void deletePhoto() {

        _photoPath = "";
        ui_imvPhoto.setImageDrawable(null);
    }

    public void onSuccessSignup() {

        gotoSignInActivity();

    }


    private void gotoSignInActivity() {

        closeProgress();


        Log.d("SingUp===Photourl+==========>>",_photoPath);

        Commons.g_user.set_photoUrl(_photoPath);

        Preference.getInstance().put(this,
                PrefConst.PREFKEY_USERNAME, ui_edtUsername.getText().toString().trim());
        Preference.getInstance().put(this,
                PrefConst.PREFKEY_USERPWD, ui_edtPwd.getText().toString().trim());

        processWelcome();
    }
    public void processWelcome(){

        String url = ReqConst.SERVER_URL + ReqConst.REQ_LOGIN;

        try {
            String username = ui_edtUsername.getText().toString().trim().replace(" ","%20");
            username = URLEncoder.encode(username,"utf-8");

            String password = ui_edtPwd.getText().toString().trim().replace(" ","%20");
            password = URLEncoder.encode(password,"utf-8");

            int device_type = 0;

            String params = String.format("/%s/%s/%d", username, password, device_type);
            url += params ;

            Log.d("loginURL===========********======>", url);

        } catch (UnsupportedEncodingException e) {

            closeProgress();
            e.printStackTrace();}

        showProgress();

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {

            @Override
            public void onResponse(String json) {

                parseWelcomeResponse(json);
            }

        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {

                closeProgress();
                showAlertDialog(getString(R.string.error));

            }
        });

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        CuponExpressApplication.getInstance().addToRequestQueue(stringRequest, url);

    }

    public void parseWelcomeResponse(String json) {

        closeProgress();

        try {
            JSONObject response = new JSONObject(json);

            int result_code = response.getInt(ReqConst.RES_CODE);

            Log.d("ResponseRESULT=====**********=========>", json);
            Log.d("RESPONS++++RESULT==========>..>>>>", String.valueOf(result_code));

            if (result_code == ReqConst.CODE_SUCCESS){

                UserEntity _user=new UserEntity();

                closeProgress();

                JSONObject user = response.getJSONObject(ReqConst.RES_USERINFO);

                _user = new UserEntity();
                int id=user.getInt(ReqConst.RES_ID);

                _user.set_idx(user.getInt(ReqConst.RES_ID));
                Log.d("id======", String.valueOf(_user.get_idx()));
                _user.set_username(user.getString(ReqConst.RES_USERNAME));
                _user.set_email(user.getString(ReqConst.RES_EMAIL));
                _user.set_password(ui_edtPwd.getText().toString());
                _user.set_photoUrl(user.getString(ReqConst.RES_PHOTO_URL));
                Log.d("whelcom===",_user.get_photoUrl());


                Preference.getInstance().put(this,
                        PrefConst.PREFKEY_USERNAME, ui_edtUsername.getText().toString().trim());

                Preference.getInstance().put(this,
                        PrefConst.PREFKEY_USEREMAIL,ui_edtEmail.getText().toString().trim());

                Preference.getInstance().put(this,
                        PrefConst.PREFKEY_USERPWD, ui_edtPwd.getText().toString().trim());

                Preference.getInstance().put(this,
                        PrefConst.PREFKEY_PHOTURL, _user.get_photoUrl());

                Log.d("photo+======signup",Commons.g_user.get_photoUrl());

                Preference.getInstance().put(this, PrefConst.PREFKEY_ID,String.valueOf(id));

                Commons.g_user = _user ;

                gotoWelcomeActivity();

            } else if(result_code == ReqConst.CODE_UNKNOWNERRO){

                closeProgress();

                showAlertDialog(getString(R.string.unregistered_user));
                Log.d("Failded ======>", valueOf(result_code));

            }else if (result_code == ReqConst.CODE_WRONGPWD){

                closeProgress();
                showAlertDialog(getString(R.string.checkPwd));
                Log.d("Failded ======>", valueOf(result_code));

            } else if (result_code == ReqConst.CODE_UNREGUSER){
                closeProgress();
                showAlertDialog(getString(R.string.unregiuser));
            }

        } catch (JSONException e) {
            closeProgress();
            e.printStackTrace();

            showAlertDialog(getString(R.string.error));
        }
    }
    private void gotoWelcomeActivity() {

        Intent intent = new Intent(this ,WelcomeActivity.class);
        startActivity(intent);
        finish();
    }



    @Override
    public void onClick(View view) {

        switch (view.getId()){

            case R.id.txv_signup:
                if (checkValid()){
                    progressSignup();}
                break;

            case R.id.imv_back:

                Intent intent = new Intent(this, SignInActivity.class);
                startActivity(intent);
                finish();
                break;

            case R.id.imv_photo:
                selectPhoto();
                break;
        }

    }

    @Override
    public void onBackPressed() {

        Intent intent = new Intent(this, SignInActivity.class);
        startActivity(intent);
        finish();

    }
}
