package com.cuponexpress.main;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.cuponexpress.CuponExpressApplication;
import com.cuponexpress.R;
import com.cuponexpress.base.CommonActivity;
import com.cuponexpress.commons.Commons;
import com.cuponexpress.model.UserEntity;
import com.cuponexpress.utils.CirculaireNetworkImageView;

public class WelcomeActivity extends CommonActivity implements View.OnClickListener {


    ImageView ui_imvPlay ;
    TextView ui_txvWecome;

    CirculaireNetworkImageView ui_imvPhoto;
    ImageLoader _imageLoader;

    UserEntity _user = null ;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);

        _imageLoader = CuponExpressApplication.getInstance().getImageLoader();

        loadLayout();
    }

    private void loadLayout() {

        ui_imvPhoto = (CirculaireNetworkImageView)findViewById(R.id.imv_photo1);
        ui_imvPhoto.setBorderColor((int)getResources().getColor(R.color.backround_color));
        ui_imvPhoto.setBorderColor2((int)getResources().getColor(R.color.white));
        ui_imvPhoto.setBorderWidth(7);
        ui_imvPhoto.setBorderWidth2(3);

        ui_txvWecome = (TextView)findViewById(R.id.txv_welcome);
        ui_txvWecome.setOnClickListener(this);
        ui_txvWecome.setText("Welcome"+" " + Commons.g_user.get_username() + "!");

        ui_imvPlay = (ImageView)findViewById(R.id.imv_play);
        ui_imvPlay.setOnClickListener(this);

        updatePhoto();

    }

    private void updatePhoto() {

       // Log.d("Photo URL=======>>>>",Commons.g_user.get_photoUrl());

        if (Commons.g_user.get_photoUrl().length() != 0 && !Commons.g_user.get_photoUrl().equals(null)){

            ui_imvPhoto.setImageUrl(Commons.g_user.get_photoUrl(),_imageLoader);

        }else {

            ui_imvPhoto.setImageResource(R.drawable.bg_non_profile);
        }

/*
        if (_user.get_photoUrl().length() > 0)
            ui_imvProfilePhoto.setImageUrl(_user.get_photoUrl(), _imageLoader);
        else
            ui_imvProfilePhoto.setImageResource(R.drawable.bg_non_profile1);*/

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){

            case R.id.imv_play:
                gotoMain();
                break;

            case R.id.txv_welcome:
                gotoMain();
                break;
        }

    }

    private void gotoMain() {

        Intent intent = new Intent(this , MainActivity.class);
        startActivity(intent);
        finish();
    }
}
