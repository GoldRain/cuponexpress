package com.cuponexpress.main;

import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.cuponexpress.R;
import com.cuponexpress.adapter.FavoViewPagerAdapter;
import com.cuponexpress.base.CommonActivity;

import java.util.ArrayList;

public class FavDetailsPagerActivity extends CommonActivity implements View.OnClickListener{

    ViewPager viewPager ;
    FavoViewPagerAdapter _favoViewPagerAdapter ;

    ArrayList<Integer> mImages = new ArrayList<>();

    TextView ui_txv_upload_img_cnt;

    ImageView ui_imvBack ;
    int _position = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fav_details_pager);

       /* mImages = getIntent().getIntegerArrayListExtra("images");
        _position = getIntent().getIntExtra("position", 0);*/

     /*   for (int i = 0 ; i < mImages.size(); i++){
            Log.d("Image url ==========>", String.valueOf(_position)) ;
        }
*/

//        Log.d("=====position",String.valueOf(selectedPosition));
       /* String str = String.valueOf(_position + 1) + "/" + String.valueOf(mImages.size());
        CharSequence chr = str;*/

/*
        ui_txv_upload_img_cnt = (TextView)findViewById(R.id.txv_upload_img_cnt);
        ui_txv_upload_img_cnt.setText(chr);

        Log.d("counter=======>>", String.valueOf(str));
*/

        initViewPager();
    }

    private void initViewPager() {

      //  LayoutInflater inflater  = (LayoutInflater)

       /* ui_txv_upload_img_cnt = (TextView)findViewById(R.id.txv_upload_img_cnt);*/

        ui_imvBack = (ImageView)findViewById(R.id.imv_back);
        ui_imvBack.setOnClickListener(this);

        _favoViewPagerAdapter = new FavoViewPagerAdapter(this/*,mImages*/);

        viewPager = (ViewPager)findViewById(R.id.vip_upload_img);

        viewPager.setAdapter(_favoViewPagerAdapter);
        /*viewPager.setCurrentItem(_position);*/

        Log.d("========",String.valueOf(_position));

     /*   viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {}

            @Override
            public void onPageSelected(int position) {

                String str = String.valueOf(position+1) + "/" + String.valueOf(mImages.size());


                CharSequence chr = str;
                ui_txv_upload_img_cnt.setText(chr);
            }

            @Override
            public void onPageScrollStateChanged(int state) {}
        });*/
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){

            case R.id.imv_back:
                finish();
                break;
        }

    }
}
