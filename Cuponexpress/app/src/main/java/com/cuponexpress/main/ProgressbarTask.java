package com.cuponexpress.main;

import android.os.Handler;
import android.view.View;



/**
 * Created by ToSuccess on 10/30/2016.
 */

public class ProgressbarTask implements Runnable {


    Handler mHandler = new Handler();
    public OnProgressBarListener mProgressBarListener;
    public View mView;

    public ProgressbarTask(View v, OnProgressBarListener mOnProgressBarListener) {

        this.mProgressBarListener = mOnProgressBarListener ;
        this.mView = v ;
    }

    interface OnProgressBarListener {
        public void setProgress(int value, View v);
        public void onProgressFinish(View v);
    }

    @Override
    public void run() {
        for (int i = 0; i <= 1000; i++) {
            final int value = i;
            try {
                Thread.sleep(10);
            } catch (Exception e) {
                e.printStackTrace();
            }
            mHandler.post(new Runnable() {

                @Override
                public void run() {
                    mProgressBarListener.setProgress(value, mView);
                }
            });

        }
        mHandler.post(new Runnable() {

            @Override
            public void run() {
                mProgressBarListener.onProgressFinish(mView);

            }
        });

    }

}
