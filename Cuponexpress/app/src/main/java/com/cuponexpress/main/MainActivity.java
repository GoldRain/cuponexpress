package com.cuponexpress.main;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.cuponexpress.CuponExpressApplication;
import com.cuponexpress.R;
import com.cuponexpress.base.CommonActivity;
import com.cuponexpress.commons.Commons;
import com.cuponexpress.fragments.AyudaFragment;
import com.cuponexpress.fragments.BellizaFragment;
import com.cuponexpress.fragments.CambiarFragment;
import com.cuponexpress.fragments.CategoriasFragment;
import com.cuponexpress.fragments.ConfigrationFragment;
import com.cuponexpress.fragments.CursosFragment;
import com.cuponexpress.fragments.DeporteFragment;
import com.cuponexpress.fragments.DetailShowFragment;
import com.cuponexpress.fragments.EntretenimientoFragment;
import com.cuponexpress.fragments.FavoriteFragment;
import com.cuponexpress.fragments.GastronFragment;
import com.cuponexpress.fragments.GeolocationFragment;
import com.cuponexpress.fragments.HomeFragment;
import com.cuponexpress.fragments.HorgarFragment;
import com.cuponexpress.fragments.InformarFragment;
import com.cuponexpress.fragments.MisCuponesFragment;
import com.cuponexpress.fragments.RegistrarFragment;
import com.cuponexpress.fragments.ShoppingFragment;
import com.cuponexpress.fragments.ShowFragment;
import com.cuponexpress.fragments.TecnologiaFragment;
import com.cuponexpress.fragments.TerminosFragment;
import com.cuponexpress.fragments.TurismoFragment;
import com.cuponexpress.fragments.VehiculosFragment;
import com.cuponexpress.preference.PrefConst;
import com.cuponexpress.preference.Preference;
import com.cuponexpress.utils.CirculaireNetworkImageView;

import java.util.Locale;

import static com.cuponexpress.R.id.frm_container;

public class MainActivity extends CommonActivity implements View.OnClickListener, NavigationView.OnNavigationItemSelectedListener {

    CirculaireNetworkImageView ui_imvPhoto;
    ImageLoader _imageLoader;

    NavigationView ui_drawer_menu;
    DrawerLayout ui_drawerlayout;
    ActionBarDrawerToggle drawerToggle;

    ImageView ui_imv_call_drawer, ui_imvSearch ,ui_imvCancel ;
    EditText ui_edtSearch ;

    TextView ui_userName ;
    public static final int MY_PERMISSIONS_REQUEST_READ_PHONE_STATE = 101;

    public LinearLayout ui_lyt_location, ui_lyt_home, ui_lyt_favo, ui_lyt_category;
    public RelativeLayout ui_ryt_main, ui_ryt_search ,ui_ryt_categorias ,ui_ryt_show ;
    public RelativeLayout ui_ryt_conf;
    View header_view;

    ImageView ui_imvHome, ui_imvLocation, ui_imvFavorite, ui_imvCategory;
    TextView ui_txvHome, ui_txvLocation, ui_txvFavorite, ui_txvCategory;

    MenuItem ui_mniCategories, ui_mniGeolocation, ui_mniConfig,ui_mniCupon, ui_mniFavo, ui_mniHome, ui_mniLogout; // First Visible

    int category = 0 , search = 0;
    String _title = "";
//    static int status = 0 ;

    LinearLayout ui_lytTabBar;



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        _imageLoader = CuponExpressApplication.getInstance().getImageLoader();

        loadLayout();

        /*if (savedInstanceState == null) {
            ui_drawer_menu.getMenu().performIdentifierAction(R.id.lyt_home, 0);}*/
    }

    public void loadLayout() {

        Commons.status = 0;

        ui_txvLocation = (TextView)findViewById(R.id.txv_location);
        ui_imvHome = (ImageView)findViewById(R.id.imv_home);
        ui_txvHome = (TextView)findViewById(R.id.txv_home);
        ui_imvLocation = (ImageView)findViewById(R.id.imv_location);
        ui_imvFavorite = (ImageView)findViewById(R.id.imv_favo);
        ui_txvFavorite = (TextView)findViewById(R.id.txv_favo);
        ui_imvCategory = (ImageView)findViewById(R.id.imv_category);
        ui_txvCategory = (TextView)findViewById(R.id.txv_category);

        ui_lyt_home = (LinearLayout)findViewById(R.id.lyt_home);
        ui_lyt_home.setOnClickListener(this);

        ui_lyt_location = (LinearLayout)findViewById(R.id.lyt_location);
        ui_lyt_location.setOnClickListener(this);

        ui_lyt_favo = (LinearLayout)findViewById(R.id.lyt_favo);
        ui_lyt_favo.setOnClickListener(this);

        ui_lyt_category = (LinearLayout)findViewById(R.id.lyt_categry);
        ui_lyt_category.setOnClickListener(this);

        //tab bar hidden

        ui_lytTabBar = (LinearLayout)findViewById(R.id.lyt_tabar);
        ui_lytTabBar.setOnClickListener(this);

        //


        ui_imvHome = (ImageView)findViewById(R.id.imv_home);
        ui_imvHome.setImageResource(R.drawable.home3_100px);

        ui_txvHome = (TextView)findViewById(R.id.txv_home);
        ui_txvHome.setTextColor(getResources().getColor(R.color.text_color));

        //Title Bar Setting
        ui_ryt_main = (RelativeLayout)findViewById(R.id.ryt_main);
        ui_ryt_conf = (RelativeLayout)findViewById(R.id.ryt_config);

        ui_ryt_search = (RelativeLayout)findViewById(R.id.ryt_search);
        ui_ryt_search.setOnClickListener(this);

        ui_ryt_categorias = (RelativeLayout)findViewById(R.id.ryt_cat);
        ui_ryt_categorias.setOnClickListener(this);

        ui_ryt_show = (RelativeLayout)findViewById(R.id.ryt_show);
        ui_ryt_show.setOnClickListener(this);
        //

        ui_drawer_menu = (NavigationView)findViewById(R.id.drawer_menu);
        ui_drawer_menu.setNavigationItemSelectedListener(this);
        header_view = ui_drawer_menu.getHeaderView(0);

        ui_imvPhoto = (CirculaireNetworkImageView)header_view.findViewById(R.id.imv_photo_h);
        ui_userName = (TextView)header_view.findViewById(R.id.txv_name_det);

        ui_imvPhoto.setBorderColor((int)getResources().getColor(R.color.theme_color));
        ui_imvPhoto.setBorderColor2((int)getResources().getColor(R.color.white));
        ui_imvPhoto.setBorderWidth(5);
        ui_imvPhoto.setBorderWidth2(2);


        ui_drawerlayout = (DrawerLayout)findViewById(R.id.drawerlayout);

        ui_imv_call_drawer = (ImageView)findViewById(R.id.imv_call_drawer);
        ui_imv_call_drawer.setOnClickListener(this);

        ui_imvSearch = (ImageView)findViewById(R.id.imv_search);
        ui_imvSearch.setOnClickListener(this);

        ui_imvCancel = (ImageView)findViewById(R.id.imv_cancel);
        ui_imvCancel.setOnClickListener(this);

        ui_edtSearch = (EditText)findViewById(R.id.edt_search);

        //  First Visible MenuItem id
        ui_mniHome=(MenuItem)ui_drawer_menu.getMenu().findItem(R.id.nav_home);
        ui_mniCategories = (MenuItem)ui_drawer_menu.getMenu().findItem(R.id.nav_category);
        ui_mniGeolocation = ui_drawer_menu.getMenu().findItem(R.id.nav_geolocation);
        ui_mniConfig = ui_drawer_menu.getMenu().findItem(R.id.nav_configuration);
        ui_mniCupon = ui_drawer_menu.getMenu().findItem(R.id.nav_miscupones);
        ui_mniFavo = ui_drawer_menu.getMenu().findItem(R.id.nav_favo);
        ui_mniLogout = ui_drawer_menu.getMenu().findItem(R.id.nav_logout);

        //  end
        checkLocationPermission();

        /*getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);*/
        setupNavigationBar();
        drawerToggle.syncState();
        homeFragment();

        titleBarChange(View.VISIBLE, View.GONE, View.GONE ,View.GONE,View.GONE);  /*TitleBarChange setting*/
        ui_drawerlayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);  /*drawermenu initial unlock method setting*/
        ui_lytTabBar.setVisibility(View.VISIBLE);  /*Tab bar initial visible setting*/


        ui_edtSearch.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence sequence, int start, int before, int count) {

                if (ui_edtSearch.getText().length() > 0){

                    ui_imvCancel.setVisibility(View.VISIBLE);

                    return ;

                }else {
                    ui_imvCancel.setVisibility(View.GONE);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

                String name = ui_edtSearch.getText().toString().toLowerCase(Locale.getDefault());
//                _adpater.filter(name);
            }
        });

        photoUpdate();
    }

    private void photoUpdate() {
        if ((Commons.g_user.toString().length() > 0)){

            Log.d("Photo URL ===========>", Commons.g_user.get_photoUrl());

            ui_userName.setText(Commons.g_user.get_username());
        }

        if (Commons.g_user.get_photoUrl().length() != 0 && !Commons.g_user.get_photoUrl().equals(null)){

            ui_imvPhoto.setImageUrl(Commons.g_user.get_photoUrl(),_imageLoader);
            ui_userName.setText(Commons.g_user.get_username());

        }else {

            ui_imvPhoto.setImageResource(R.drawable.bg_non_profile);
            ui_userName.setText("User name");

        }
    }
                                 /*End LoadLayout*/

    private void setupNavigationBar() {

        drawerToggle = new ActionBarDrawerToggle(this, ui_drawerlayout, R.string.drawer_open, R.string.drawer_close) {
            @Override
            public void onDrawerOpened(View drawerView) {

                super.onDrawerOpened(drawerView);}

            @Override
            public void onDrawerClosed(View drawerView) {

                super.onDrawerClosed(drawerView);}
        };

        ui_drawerlayout.setDrawerListener(drawerToggle);

    }


      /*NavigationView*/


    @Override
    public boolean onNavigationItemSelected( MenuItem item) {

        int id = item.getItemId();

        /*if (item.isChecked()) item.setChecked(false); else item.setChecked(true);*/
        ui_drawerlayout.closeDrawers();

        switch (id) {

            // CastronVisible

            case R.id.nav_home:

                homeFragment();
               // item.setChecked(true);
                break;

            case R.id.nav_category:
                categoryFragment();

                //category = 1 ;
                break;

            case R.id.nav_geolocation:

                geolocationFragment();
                break;

            case R.id.nav_configuration:

                ui_lytTabBar.setVisibility(View.VISIBLE);
                configFragment();
                break;

            case R.id.nav_miscupones:
                ui_lytTabBar.setVisibility(View.GONE);
                cuponesFragment();
                Commons.status = 1;
                break;

            case R.id.nav_favo:
                ui_lytTabBar.setVisibility(View.VISIBLE);
                favoritsFragment();
//                Commons.status = 4;
                break;

            case R.id.nav_logout:
                logout();
                break;
        }

        return true;
    }

    public void categoryFragment() {

        initIcon();
        titleBarChange(View.VISIBLE, View.GONE , View.GONE, View.GONE,View.GONE);

        navigationSelected(false, false, false, true);

        ui_imvCategory.setImageResource(R.drawable.tab_categories_on);
        ui_txvCategory.setTextColor(getResources().getColor(R.color.white));

        //ui_lytTabBar.setVisibility(View.GONE);
        CategoriasFragment fragment = new CategoriasFragment(this);
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(frm_container, fragment).commit();
    }

    public void homeFragment(){

        Commons.status = 0;
        initIcon();
        ui_imvHome.setImageResource(R.drawable.home3_100px);
        ui_txvHome.setTextColor(getResources().getColor(R.color.white));

        titleBarChange(View.VISIBLE, View.GONE, View.GONE, View.GONE ,View.GONE);

        ui_lytTabBar.setVisibility(View.VISIBLE);

        ui_drawerlayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);   // set enable Drawer

    /*navigation select*/
        navigationSelected(true,false,false,false);

        HomeFragment fragment = new HomeFragment(this);
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(frm_container, fragment).commit();

    }

    public void geolocationFragment(){

        Commons.status = 0;
        initIcon();
        ui_imvLocation.setImageResource(R.drawable.location3_100px);
        ui_txvLocation.setTextColor(getResources().getColor(R.color.white));
        ui_lytTabBar.setVisibility(View.VISIBLE);

        titleBarChange(View.VISIBLE, View.GONE, View.GONE, View.GONE ,View.GONE);

        ui_drawerlayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);  // set enable Drawer

            /*navigation select*/
        navigationSelected(false,true,false,false);

        GeolocationFragment fragment = new GeolocationFragment(this);
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(frm_container, fragment);
        fragmentTransaction.commit();
    }

    public void favoritsFragment() {

        Commons.status = 4;
        initIcon();
        titleBarChange(View.VISIBLE, View.GONE, View.GONE, View.GONE ,View.GONE);

        ui_imvFavorite.setImageResource(R.drawable.like3_100px);
        ui_txvFavorite.setTextColor(getResources().getColor(R.color.white));

        ui_drawerlayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);  // set enable Drawer

    /*navigation select*/
        navigationSelected(false,false,true,false);

        FavoriteFragment fragment = new FavoriteFragment(this);
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(frm_container,fragment).commit();
    }

    private void logout() {

        Intent intent = new Intent(this , SignInActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        Preference.getInstance().put(this, PrefConst.PREFKEY_USEREMAIL, "");
        Preference.getInstance().put(this, PrefConst.PREFKEY_USERPWD, "");
        Preference.getInstance().put(this, PrefConst.PREFKEY_USERNAME, "");
        startActivity(intent);
        finish();

    }

//    CONFIGURACION FRAGMENT_START

    public void configFragment() {

        initIcon();

        ui_lytTabBar.setVisibility(View.GONE);

        ui_drawerlayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);  //drawer visible
        titleBarChange(View.GONE, View.VISIBLE, View.GONE, View.GONE ,View.GONE);  //titleBar visiable setting
        ui_lytTabBar.setVisibility(View.VISIBLE);           //tabBar visable setting


     /*navigation select*/
        navigationSelected(false,false,false,true);

        ConfigrationFragment fragment = new ConfigrationFragment(this);
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(frm_container, fragment).commit();
    }

    public void registrarFragment(){

        titleBarChange(View.GONE , View.VISIBLE, View.GONE, View.GONE ,View.GONE);
        ui_lytTabBar.setVisibility(View.GONE);

        RegistrarFragment fragment = new RegistrarFragment(this);
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(frm_container, fragment).commit();

    }

    public void cambiarFragment() {

        titleBarChange(View.GONE, View.VISIBLE, View.GONE, View.GONE ,View.GONE);
        ui_lytTabBar.setVisibility(View.GONE);

        CambiarFragment fragment = new CambiarFragment(this);
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(frm_container, fragment).commit();

    }

    public void terminosFragment() {

        titleBarChange(View.GONE, View.VISIBLE, View.GONE, View.GONE ,View.GONE);
        ui_lytTabBar.setVisibility(View.GONE);

        TerminosFragment fragment = new TerminosFragment(this);
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(frm_container, fragment).commit();

    }

    public void informarFragment() {

        titleBarChange(View.GONE, View.VISIBLE, View.GONE, View.GONE ,View.GONE);
        ui_lytTabBar.setVisibility(View.GONE);

        InformarFragment fragment = new InformarFragment(this);
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(frm_container, fragment).commit();
    }

    public void ayudaFragment() {


        titleBarChange(View.GONE, View.VISIBLE, View.GONE, View.GONE ,View.GONE);
        ui_lytTabBar.setVisibility(View.GONE);

        AyudaFragment fragment = new AyudaFragment(this);
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(frm_container, fragment).commit();
    }

    // REGISTAR FRAG_END

     /*CATEGORIAS START*/
    public void gastronoFragment(){

        initIcon();
        titleBarChange(View.GONE , View.GONE, View.GONE, View.VISIBLE,View.GONE);
        ui_lytTabBar.setVisibility(View.GONE);

        GastronFragment fragment = new GastronFragment(this);
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(frm_container, fragment).commit();
    }

    public void bellizaFragment(){

        initIcon();
        titleBarChange(View.GONE , View.GONE, View.GONE, View.VISIBLE,View.GONE);
        ui_lytTabBar.setVisibility(View.GONE);

        BellizaFragment fragment = new BellizaFragment(this);
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(frm_container, fragment).commit();
    }

    public void turismoFragment() {

        initIcon();
        titleBarChange(View.GONE, View.GONE, View.GONE, View.VISIBLE,View.GONE);
        ui_lytTabBar.setVisibility(View.GONE);

        TurismoFragment fragment = new TurismoFragment(this);
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(frm_container, fragment).commit();
    }

    public void deporteFragment(){
        initIcon();
        titleBarChange(View.GONE , View.GONE, View.GONE, View.VISIBLE,View.GONE);
        ui_lytTabBar.setVisibility(View.GONE);

        DeporteFragment fragment = new DeporteFragment(this);
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(frm_container, fragment).commit();
    }

    public void entretenFragment(){

        initIcon();
        titleBarChange(View.GONE, View.GONE,View.GONE, View.VISIBLE,View.GONE);
        ui_lytTabBar.setVisibility(View.GONE);

        EntretenimientoFragment fragment = new EntretenimientoFragment(this);
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(frm_container, fragment).commit();
    }

    public void shoppingFragment() {

        initIcon();
        titleBarChange(View.GONE,View.GONE ,View.GONE, View.VISIBLE,View.GONE);
        ui_lytTabBar.setVisibility(View.GONE);

        ShoppingFragment fragment = new ShoppingFragment(this);
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction() ;
        fragmentTransaction.replace(frm_container, fragment).commit();
    }

    public void vehiculosFragment(){

        initIcon();
        titleBarChange(View.GONE, View.GONE, View.GONE, View.VISIBLE,View.GONE);
        ui_lytTabBar.setVisibility(View.GONE);

        VehiculosFragment fragment = new VehiculosFragment(this);
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(frm_container, fragment).commit();


    }

    public void horgarFragment(){

        initIcon();
        titleBarChange(View.GONE, View.GONE, View.GONE, View.VISIBLE,View.GONE);
        ui_lytTabBar.setVisibility(View.GONE);

        HorgarFragment fragment = new HorgarFragment(this);
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(frm_container, fragment).commit();

    }

    public void cursosFragment(){

        initIcon();
        titleBarChange(View.GONE , View.GONE, View.GONE, View.VISIBLE,View.GONE);
        ui_lytTabBar.setVisibility(View.GONE);

        CursosFragment fragment = new CursosFragment(this);
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(frm_container, fragment).commit();
    }

    public void tecnologFragment(){

        initIcon();
        titleBarChange(View.GONE , View.GONE, View.GONE, View.VISIBLE,View.GONE);
        ui_lytTabBar.setVisibility(View.GONE);

        TecnologiaFragment fragment = new TecnologiaFragment(this);
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(frm_container, fragment).commit();

    }
    /*//////////////////////////// END //////////////////////////////////*/

    public void cuponesFragment() {

        initIcon();

        titleBarChange(View.VISIBLE, View.GONE, View.GONE, View.GONE,View.GONE);

        MisCuponesFragment fragment = new MisCuponesFragment(this);
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(frm_container, fragment).commit() ;
    }

    public void showFragment(){

        initIcon();

        titleBarChange(View.GONE, View.GONE,View.GONE, View.GONE,View.VISIBLE);

        ui_lytTabBar.setVisibility(View.GONE);
        ui_drawerlayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);

        ShowFragment fragment = new ShowFragment(this);
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(frm_container, fragment).commit();
    }

    public void detailShowFragment(){

        initIcon();

        titleBarChange(View.GONE , View.GONE, View.GONE, View.GONE,View.GONE);
        ui_drawerlayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);

        ui_lytTabBar.setVisibility(View.GONE);

        DetailShowFragment fragment = new DetailShowFragment(this);
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(frm_container, fragment).commit();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.imv_call_drawer:

                ui_drawerlayout.openDrawer(Gravity.LEFT);
                break;

            case R.id.imv_search:

                titleBarChange(View.GONE , View.GONE, View.VISIBLE, View.GONE,View.GONE);
                search = 1;
                break;

            case R.id.imv_cancel:
                ui_edtSearch.setText("");
                break;

            case R.id.lyt_location:
                geolocationFragment();
                break;

            case R.id.lyt_home:

                homeFragment();
                break;

            case R.id.lyt_favo:
               favoritsFragment();
                break;

            case R.id.lyt_categry:
                categoryFragment();
                break;

        }
    }

    private void initIcon() {

        ui_imvHome.setImageResource(R.drawable.home2_100px);
        ui_txvHome.setTextColor(getResources().getColor(R.color.text_color));
        ui_imvLocation.setImageResource(R.drawable.location2_100px);
        ui_txvLocation.setTextColor(getResources().getColor(R.color.text_color));
        ui_imvFavorite.setImageResource(R.drawable.like2_100px);
        ui_txvFavorite.setTextColor(getResources().getColor(R.color.text_color));
        ui_imvCategory.setImageResource(R.drawable.tab_categories_off);
        ui_txvCategory.setTextColor(getResources().getColor(R.color.text_color));
    }

    public void titleBarChange(int a, int b, int c, int d ,int e){

        ui_ryt_main.setVisibility(a);
        ui_ryt_conf.setVisibility(b);
        ui_ryt_search.setVisibility(c);
        ui_ryt_categorias.setVisibility(d);
        ui_ryt_show.setVisibility(e);
    }

    public void navigationSelected(boolean flag1, boolean flag2, boolean flag3, boolean flag4){

         /*navigation select*/
        ui_mniHome.setChecked(flag1);
        ui_mniGeolocation.setChecked(flag2);
        ui_mniFavo.setChecked(flag3);
        ui_mniCategories.setChecked(flag4);

    }

    @Override
    public void onBackPressed() {

        if (this.ui_drawerlayout.isDrawerOpen(GravityCompat.START)) {
            this.ui_drawerlayout.closeDrawer(GravityCompat.START);

        } else if ( (search == 1) ){

            titleBarChange(View.VISIBLE, View.GONE, View.GONE, View.GONE,View.GONE);
            search = 0;

        } else if (category == 1 ){

            homeFragment();
            category = 0 ;

        } else {
            onExit();

//          super.onBackPressed();

        }
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {

            case MY_PERMISSIONS_REQUEST_READ_PHONE_STATE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay!

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }


    public static boolean hasPermissions(Context context, String... permissions) {

        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {

            for (String permission : permissions) {

                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    public void checkLocationPermission() {

        String[] PERMISSIONS = {Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.WRITE_EXTERNAL_STORAGE,Manifest.permission.RECEIVE_BOOT_COMPLETED,Manifest.permission.READ_PHONE_STATE,
                Manifest.permission.CALL_PHONE,Manifest.permission.SEND_SMS};

        if (hasPermissions(this, PERMISSIONS)){

        }else {
            ActivityCompat.requestPermissions(this, PERMISSIONS, 101);
        }

    }




}
